package com.koorca.viewpager;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.koorca.MyApplication;
import com.koorca.R;
import com.koorca.ValidateNumber;
import com.koorca.VerificationCode;
import com.koorca.customclasses.CustomBoldText;
import com.koorca.customclasses.CustomMediumText;
import com.koorca.interfaces.ValidateClass;
import com.koorca.modal.CountryListModal;
import com.koorca.utilities.CommonUtility;
import com.koorca.utilities.Constants;
import com.koorca.utilities.CustomProgressDialog;
import com.koorca.utilities.VerificationCodeAPI;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by sourav on 5/9/16.
 */

public class PageSeven extends Fragment  {
    View view;
    CustomProgressDialog pDialog;
    CustomMediumText mCountry, mCountryCode;
    CustomBoldText mContinue;
    EditText mMobileNumber;
    CommonUtility utility;
    SharedPreferences sPref;
    SharedPreferences.Editor editor;
    Gson gson;
    Type type;
    ArrayList<CountryListModal> alList;
    CharSequence[] countries;
    String sMobileNumber, sCode, sResponse;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.pager_item_seven, null);
     /*   pDialog = new CustomProgressDialog(getActivity(), "Please wait...", false);
        utility = new CommonUtility(getActivity());
        sPref = getActivity().getSharedPreferences(Constants.SHARED_PREF, MODE_PRIVATE);
        editor = sPref.edit();

        mCountry = (CustomMediumText) view.findViewById(R.id.tvCountry1);
        mCountryCode = (CustomMediumText) view.findViewById(R.id.tvCountryCode1);
        mMobileNumber = (EditText) view.findViewById(R.id.tvMobileNumber1);
        mContinue = (CustomBoldText) view.findViewById(R.id.tvContinue1);
        mMobileNumber.setTypeface(MyApplication.typefaceMedium);
        mCountry.setOnClickListener(this);
        mContinue.setOnClickListener(this);*/


        return view;
    }


    /*@Override
    public void onResume() {
        super.onResume();
        gson = new Gson();
        type = new TypeToken<ArrayList<CountryListModal>>() {
        }.getType();
        alList = gson.fromJson(sPref.getString(Constants.CITY_LIST, ""), type);
        alList = new ArrayList<>();
        if (alList == null || alList.size() == 0) {
            if (utility.checkInternetConection()) {
                pDialog.showDialog();
                callAPICountry();
            } else {
                showSettingsAlert();

            }

        }
    }

    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(WelcomeFragmentActivity.activity);

        alertDialog.setTitle("SETTINGS");

        alertDialog
                .setMessage("Please connect to internet...");
        alertDialog.setCancelable(false);

        alertDialog.setPositiveButton("Settings",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));


                    }
                });


        alertDialog.show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvCountry1:
                showCountriesList();

                break;

            case R.id.tvContinue1:
                if (utility.checkInternetConection()) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    sMobileNumber = mMobileNumber.getText().toString().trim();
                    sCode = mCountryCode.getText().toString().trim();
                    if (sCode.length() == 0) {
                        Toast.makeText(getActivity(), "Fill the code by select the city.", Toast.LENGTH_LONG).show();
                    } else if (sMobileNumber.length() == 0) {
                        Toast.makeText(getActivity(), "Enter the mobile number", Toast.LENGTH_LONG).show();

                    } else if (sMobileNumber.length() < 8 || sMobileNumber.length() > 10) {
                        Toast.makeText(getActivity(), "Mobile Number should be between 8 to 10 digits.", Toast.LENGTH_LONG).show();

                    } else {
                        callAPI();
                    }

                } else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.connect_to_internet), Toast.LENGTH_LONG).show();
                }

                break;
        }
    }


    private void showCountriesList() {

        countries = new CharSequence[alList.size()];
        for (int i = 0; i < alList.size(); i++) {
            CountryListModal name = alList.get(i);
            countries[i] = name.getCountryName();
        }
        AlertDialog.Builder alertdialogbuilder = new AlertDialog.Builder(getActivity(), R.style.MyDialogTheme);


        alertdialogbuilder.setTitle("Select Country ");

        alertdialogbuilder.setItems(countries, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mCountry.setText(alList.get(which).getCountryName());
                mCountryCode.setText(alList.get(which).getCountryCode());
                editor.putString(Constants.EMERGENCY_NUMBER, alList.get(which).getCountryPoliceNumber()).commit();


            }
        });

        AlertDialog dialog = alertdialogbuilder.create();

        dialog.show();


    }

    private void callAPI() {
        if (utility.checkInternetConection()) {
            pDialog.showDialog();
            VerificationCodeAPI.getOTP(sCode + sMobileNumber, new ValidateClass() {
                @Override
                public void returnMessage(String message) {
                    editor.putString(Constants.MOBILE_NUMBER, sCode + sMobileNumber).commit();

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent i = new Intent(getActivity(), VerificationCode.class);
                            startActivity(i);
*//*
                            overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
*//*
                            pDialog.hideDialog();

                        }
                    }, 1000);


                }
            });

        } else {
            pDialog.hideDialog();

            Toast.makeText(getActivity(), getResources().getString(R.string.connect_to_internet), Toast.LENGTH_LONG).show();
        }

    }

    private void callAPICountry() {
        String url = "http://koorca.s4.staging-host.com/KOORCA/GetCountryList";


        StringRequest strReq = new StringRequest(Request.Method.GET,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("Result", response.toString());
                try {
                    saveCountryList(response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("error", "Error: " + error.getMessage());
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Accept", "application/json");

                return headers;
            }
        };
        MyApplication.getInstance().addToRequestQueue(strReq, "country_name");

    }

    private void saveCountryList(String response) throws JSONException {

        JSONArray jsonArray = new JSONArray(response);
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            CountryListModal code = new CountryListModal();
            code.setCountryId(jsonObject.getString("CountryId"));
            code.setCountryName(jsonObject.getString("CountryName"));
            code.setCountryCode(jsonObject.getString("CountryCode"));
            code.setCountryPoliceNumber(jsonObject.getString("CountryPoliceCode"));
            alList.add(code);
        }
        pDialog.hideDialog();
    }*/
}


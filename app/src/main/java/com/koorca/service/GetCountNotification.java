package com.koorca.service;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.koorca.MyApplication;
import com.koorca.utilities.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import me.leolin.shortcutbadger.ShortcutBadger;

/**
 * Created by sourav on 17/11/16.
 */

public class GetCountNotification extends Service {
    SharedPreferences sPref;
    SharedPreferences.Editor editor;

    @Override
    public void onCreate() {
        super.onCreate();
        sPref = getSharedPreferences(Constants.SHARED_PREF, MODE_PRIVATE);
        editor = sPref.edit();
        callAPI();
    }

    private void callAPI() {
        String deviceId = sPref.getString(Constants.DEVICE_ID, "");
        String url = "http://koorca.s4.staging-host.com/KOORCA/GetCountList?deviceId=" + deviceId;
        ;


        StringRequest strReq = new StringRequest(Request.Method.GET,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("Result", response.toString());
                try {
                    responseParse(response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("error", "Error: " + error.getMessage());

            }
        }) {


        };
        MyApplication.getInstance().addToRequestQueue(strReq, "get_otp");
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void responseParse(String response) throws JSONException {
        JSONObject jsonObject = new JSONObject(response);
        if (jsonObject.getBoolean("status")) {
          String count=  jsonObject.getString("Count");
            editor.putString(Constants.NOTIFICATION_COUNT, count).commit();
            Intent intent = new Intent("com.koorca.notification_come");
            // You can also include some extra data.
            intent.putExtra("notification_count",count);
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);

            int badgeCount = Integer.parseInt(count);
            ShortcutBadger.applyCount(this, badgeCount);
            stopSelf();
        }
    }
}

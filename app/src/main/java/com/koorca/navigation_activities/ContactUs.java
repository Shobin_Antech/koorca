package com.koorca.navigation_activities;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.koorca.MyApplication;
import com.koorca.R;
import com.koorca.customclasses.CustomBoldText;
import com.koorca.service.UploadImagesToServer;
import com.koorca.utilities.CommonUtility;
import com.koorca.utilities.Constants;
import com.koorca.utilities.CustomProgressDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import me.leolin.shortcutbadger.ShortcutBadger;

import static com.android.volley.VolleyLog.TAG;

/**
 * Created by sourav on 8/11/16.
 */

public class ContactUs extends Activity {
    CustomProgressDialog mDialog;
    ImageView mBack;
    CustomBoldText mContact;
    EditText mName, mEmail, mMessage;
    String name, email, message;
    View view1, view2, view3;
    RelativeLayout mainRL;
    CommonUtility utility;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact_us);

        mDialog = new CustomProgressDialog(this, "Please wait...", false);
        mBack = (ImageView) findViewById(R.id.ivBack);
        mContact = (CustomBoldText) findViewById(R.id.tvContact);
        mName = (EditText) findViewById(R.id.etName);
        mEmail = (EditText) findViewById(R.id.etEmail);
        mMessage = (EditText) findViewById(R.id.etMessage);
        view1 = (View) findViewById(R.id.view1);
        view2 = (View) findViewById(R.id.view2);
        view3 = (View) findViewById(R.id.view3);
        mainRL = (RelativeLayout) findViewById(R.id.mainRL);
        utility = new CommonUtility(this);
        //openDialog();

    }

    private void openDialog() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.update_app);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);

        dialog.show();

    }

    public void hitAPI(View view) {
        if (utility.checkInternetConection()) {
            name = mName.getText().toString();
            email = mEmail.getText().toString();
            message = mMessage.getText().toString();
            getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
            );
            if (name.equals("") && email.equals("") && message.equals("")) {

                Snackbar.make(mainRL, "Please fill all the details", Snackbar.LENGTH_SHORT).show();
            } else if (name.equals("")) {
                Snackbar.make(mainRL, "Please enter the name", Snackbar.LENGTH_SHORT).show();

            } else if (email.equals("")) {
                Snackbar.make(mainRL, "Please enter the email", Snackbar.LENGTH_SHORT).show();

            } else if (!CommonUtility.isValidEmail(email)) {
                Snackbar.make(mainRL, "Please enter the valid Email Id", Snackbar.LENGTH_SHORT).show();


            } else if (message.equals("")) {
                Snackbar.make(mainRL, "Please enter the message", Snackbar.LENGTH_SHORT).show();

            } else {
                mDialog.showDialog();
                callAPI(name, email, message);
                hideKeyboard(view);

            }
        } else {
            Snackbar.make(mainRL, getResources().getString(R.string.connect_to_internet), Snackbar.LENGTH_SHORT).show();

        }


    }

    private void callAPI(final String name, final String email, final String message) {
        mDialog.showDialog();
        String url = "http://koorca.s4.staging-host.com/Koorca/UserFeedback";
        StringRequest volleyRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("notificationsResponse", response);
                        try {
                            parseResponse(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                            Toast.makeText(ContactUs.this, "Timeout",
                                    Toast.LENGTH_LONG).show();
                        } else if (error instanceof AuthFailureError) {
                            //TODO
                            Toast.makeText(ContactUs.this, "AuthFailureError",
                                    Toast.LENGTH_LONG).show();
                        } else if (error instanceof ServerError) {
                            Toast.makeText(ContactUs.this, "ServerError",
                                    Toast.LENGTH_LONG).show();
                            //TODO
                        } else if (error instanceof NetworkError) {
                            //TODO
                            Toast.makeText(ContactUs.this, "NetworkError",
                                    Toast.LENGTH_LONG).show();
                        } else if (error instanceof ParseError) {
                            Toast.makeText(ContactUs.this, "ParseError",
                                    Toast.LENGTH_LONG).show();
                            //TODO
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params = new HashMap<String, String>();
                params.put("Name", name);
                params.put("Email", email);
                params.put("Message", message);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
/*
                params.put("Content-Type","application/json");
*/
                params.put("Accept", "application/json");
                return params;
            }
        };
        volleyRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(volleyRequest, "make_post");


























      /*
        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("Result", response.toString());





            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("error", "Error: " + error.getMessage());
                mDialog.hideDialog();



            }
        }) {

            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put("Name",name);
                params.put("Email",email);
                params.put("Message",message);


                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type","application/json");
                headers.put("Accept","application/json");

                return headers;
            }

        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(strReq, "contact_us");
*/


    }

    private void parseResponse(String response) throws JSONException {
        JSONObject jsonObject = new JSONObject(response);
        mDialog.hideDialog();
        if (jsonObject.getBoolean("status")) {
            Toast.makeText(ContactUs.this, "Feedback sent successfully", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(ContactUs.this, "Please send again..", Toast.LENGTH_SHORT).show();

        }
        mName.setText("");
        mEmail.setText("");
        mMessage.setText("");
    }


    ;


    public void onBackClick(View views) {
        finish();
        overridePendingTransition(R.anim.enter_left, R.anim.exit_right);

    }
    public  void hideKeyboard( View et){

        InputMethodManager inputManager = (InputMethodManager)ContactUs.this.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(et.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }
}

package com.koorca.utilities;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.RelativeLayout;

import com.koorca.R;


/**
 * Created by parikshit_shekhawat on 9/12/15.
 */
public class CustomProgressDialog {

    ProgressDialog dialog;
    Context con;
    String mess;
    boolean cancelable=false;
    boolean is_showing=false;

    public CustomProgressDialog(Context c, String message, boolean cancel)
    {
        this.con=c;
        this.mess=message;
        final String m=this.mess;
        this.cancelable=cancel;
        dialog = new ProgressDialog(con, R.style.MyProgressBar){
            @Override
            protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);

                getWindow().setLayout(RelativeLayout.LayoutParams.WRAP_CONTENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT);
                setContentView(R.layout.custom_loader);
            }
        };
        dialog.setIndeterminate(true);
        dialog.setCancelable(this.cancelable);

    }
    public void showDialog()
    {

        is_showing=true;
        dialog.show();

    }

    public void hideDialog()
    {
        try {
            is_showing = false;
            dialog.cancel();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public boolean isShowing()
    {
        return is_showing;
    }
}

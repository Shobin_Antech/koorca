package com.koorca;

import android.Manifest;
import android.app.Activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;

import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.koorca.customclasses.CustomBoldText;
import com.koorca.interfaces.ValidateClass;
import com.koorca.navigation_activities.MainClass;
import com.koorca.service.FusedLocationService;
import com.koorca.service.GPSTracker;
import com.koorca.utilities.CommonUtility;
import com.koorca.utilities.Constants;
import com.koorca.utilities.VerificationCodeAPI;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by sourav on 31/8/16.
 */

public class VerificationCode extends Activity implements View.OnClickListener {
    private static final int PERMISSION_REQUEST_CODES = 2;
    SharedPreferences sPref;
    SharedPreferences.Editor editor;
    ImageView mBack;
    EditText mCode;
    CustomBoldText mResend, mConfirm;
    private boolean result;
    private CommonUtility utility;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.verify_code);
        utility = new CommonUtility(this);
        sPref = getSharedPreferences(Constants.SHARED_PREF, MODE_PRIVATE);
        editor = sPref.edit();
        mBack = (ImageView) findViewById(R.id.ivBack);
        mResend = (CustomBoldText) findViewById(R.id.tvResendCode);
        mConfirm = (CustomBoldText) findViewById(R.id.tvConfirmCode);
        mCode = (EditText) findViewById(R.id.etVerificationCode);
        mConfirm.setOnClickListener(this);
        mResend.setOnClickListener(this);
        mBack.setOnClickListener(this);
       /* int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (checkPermission())
            {
                checkLocation();
            }
            else {
                requestPermission();
            }
        }*/

        checkLocation();


    }

    private boolean checkPermission() {
        int result = ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION);


        return result == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_CODES);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODES:
                if (grantResults.length > 0) {

                    boolean locationAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;


                    if (locationAccepted) {
                        Toast.makeText(getApplicationContext(), "Permission Granted, Now you can access location.", Toast.LENGTH_LONG).show();
                        startService(new Intent(this, GPSTracker.class));

                    } else {

                        Toast.makeText(getApplicationContext(), "Permission Denied, You cannot access location.", Toast.LENGTH_LONG).show();

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                            if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
                            showMessageOKCancel("You need to allow access to location  permission",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                                                    requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE},
//                                                            PERMISSION_REQUEST_CODE);
                                                Intent intent = new Intent();
                                                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                                Uri uri = Uri.fromParts("package", VerificationCode.this.getPackageName(), null);
                                                intent.setData(uri);
                                                startActivity(intent);

                                            }
                                        }
                                    });
                            return;
//                            }
                        }

                    }
                }


                break;
        }
    }


    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(VerificationCode.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .create()
                .show();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvResendCode:
                VerificationCodeAPI.getOTP(sPref.getString(Constants.MOBILE_NUMBER, ""), new ValidateClass() {
                    @Override
                    public void returnMessage(String message) {
                        Toast.makeText(VerificationCode.this, message, Toast.LENGTH_LONG).show();
                    }
                });
                break;
            case R.id.tvConfirmCode:
                int currentAPIVersion = Build.VERSION.SDK_INT;
                if (currentAPIVersion >= Build.VERSION_CODES.M) {
                    if (checkPermission()) {
                        callAPI();
                    } else {
                        requestPermission();
                    }
                } else {
                    callAPI();

                }

                break;
            case R.id.ivBack:
                finish();
                overridePendingTransition(R.anim.enter_left, R.anim.exit_right);
        }
    }

    private void callAPI() {
        String code = mCode.getText().toString().trim();
        String number = sPref.getString(Constants.MOBILE_NUMBER, "");
        String url = "http://koorca.s4.staging-host.com/KOORCA/MatchCode/" + code + "/" + number;

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("Result", response.toString());
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("message").equals("Welcome")) {
                        editor.putString(Constants.PHONE_ID, jsonObject.getString("phoneId")).commit();
                        editor.putBoolean(Constants.USER_LOGIN, true).commit();
                        if (!sPref.getBoolean(Constants.UPDATE_NUMBER, false)) {
                            Toast.makeText(VerificationCode.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();

                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    Intent i = new Intent(VerificationCode.this, TermsAndCondition.class);
                                    startActivity(i);
                                    finish();
                                    overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                                }
                            }, 1000);
                        } else {
                            Toast.makeText(VerificationCode.this, "Mobile number updated successfully.", Toast.LENGTH_SHORT).show();

                            Intent i = new Intent(VerificationCode.this, MainClass.class);
                            startActivity(i);
                            finish();
                            overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                        }

                    } else {
                        Toast.makeText(VerificationCode.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("error", "Error: " + error.getMessage());

            }
        }) {


        };
        MyApplication.getInstance().addToRequestQueue(strReq, "get_otp");
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    private void checkLocation() {
        if (utility.isGPSOn()) {
            if (utility.checkInternetConection()) {
                startService(new Intent(this, FusedLocationService.class));
            } else {
                Toast.makeText(VerificationCode.this, "Please connect to internet", Toast.LENGTH_SHORT).show();
            }

        } else {
            showSettingsAlert("Network");
        }
    }

    public void showSettingsAlert(String provider) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        alertDialog.setTitle(provider + " SETTINGS");

        alertDialog
                .setMessage(provider + " is not enabled! Want to go to settings menu?");

        alertDialog.setPositiveButton("Settings",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        Intent intent = new Intent(
                                Settings.ACTION_LOCATION_SOURCE_SETTINGS);

                        startActivity(intent);
                        dialog.cancel();
                        dialog.dismiss();

                    }
                });


        alertDialog.show();
    }

}

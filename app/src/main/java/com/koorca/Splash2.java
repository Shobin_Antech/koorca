package com.koorca;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.VideoView;


import com.google.android.gms.common.api.GoogleApiClient;

import com.koorca.navigation_activities.MainClass;
import com.koorca.service.CountryName;
import com.koorca.utilities.CommonUtility;
import com.koorca.utilities.Constants;
import com.koorca.viewpager.ViewPager2;
import com.koorca.viewpager.WelcomeFragmentActivity;

/**
 * Created by sourav on 16/9/16.
 */

public class Splash2 extends Activity {
    CommonUtility utility;
    SharedPreferences sPref;
    VideoView videoView;
    ImageView splash;
    RelativeLayout splashRL;
    private GoogleApiClient googleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if ((getIntent().getFlags() & Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) != 0) {
            finish();
            return;
        }
        setContentView(R.layout.splash2);


        sPref = getSharedPreferences(Constants.SHARED_PREF, MODE_PRIVATE);
        utility = new CommonUtility(this);
        videoView = (VideoView) findViewById(R.id.videoView);
        splash = (ImageView) findViewById(R.id.splash);
        splashRL = (RelativeLayout) findViewById(R.id.splashRL);
        int currentAPIVersion = Build.VERSION.SDK_INT;
        /*if(currentAPIVersion>=Build.VERSION_CODES.M)
        {
            splashRL.setBackgroundColor(Color.parseColor("#ffffffff"));
        }
        else
        {
            splashRL.setBackgroundColor(Color.parseColor("#ffffff"));
        }*/


        String UrlPath = "android.resource://" + getPackageName() + "/" + R.raw.crok_short;

        videoView.setVideoURI(Uri.parse(UrlPath));
        videoView.setMediaController(null);

        //callService();

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

            public void onPrepared(MediaPlayer mp) {
                splash.setVisibility(View.GONE);

            }
        });


        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                if (!sPref.getBoolean(Constants.USER_LOGIN, false)) {
                    Intent i = new Intent(Splash2.this, WelcomeFragmentActivity.class);
                    startActivity(i);
                    finish();
                } else if (!sPref.getBoolean(Constants.TERMS_CONDITIONS, false)) {
                    Intent iVerify = new Intent(Splash2.this, ValidateNumber.class);
                    startActivity(iVerify);
                    finish();
                } else if (!sPref.getBoolean(Constants.TUTORIAL_FINAL, false)) {
                    Intent iVerify = new Intent(Splash2.this, ViewPager2.class);
                    startActivity(iVerify);
                    finish();
                } else {
                    Intent i = new Intent(Splash2.this, MainClass.class);
                    startActivity(i);
                    finish();
                }

            }
        });

        videoView.start();


    }

    private void callService() {
        if (utility.checkInternetConection()) {
            startService(new Intent(Splash2.this, CountryName.class));

        } else {
            Toast.makeText(Splash2.this, getResources().getString(R.string.connect_to_internet), Toast.LENGTH_LONG).show();
        }

    }


}

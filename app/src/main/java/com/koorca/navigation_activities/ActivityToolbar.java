package com.koorca.navigation_activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.koorca.R;
import com.koorca.customclasses.CustomMediumText;
import com.koorca.customclasses.CustomRegularText;

import me.leolin.shortcutbadger.ShortcutBadger;

/**
 * Created by sourav on 10/11/16.
 */

public class ActivityToolbar extends Activity {
    ImageView ivBack;
    ImageView mSettings;
    CustomMediumText mName;
    CustomRegularText mNotificationCount;
    RelativeLayout mBellIconNotification;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.toolbar_transparent);
        ivBack = (ImageView) findViewById(R.id.ivBack);
        mSettings = (ImageView) findViewById(R.id.ivSettings);
        mName = (CustomMediumText) findViewById(R.id.tvHeading);
        mNotificationCount = (CustomRegularText) findViewById(R.id.tvNotificationCount);
        mBellIconNotification = (RelativeLayout) findViewById(R.id.rlNotification);
        mSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ActivityToolbar.this, Settings.class);
                startActivity(i);
            }
        });
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                overridePendingTransition(R.anim.enter_left, R.anim.exit_right);
            }
        });

    }

    @Override
    protected void onPause() {
        super.onPause();
        boolean success = ShortcutBadger.removeCount(ActivityToolbar.this);
        if(success)
        {

        }
        else
        {
            ShortcutBadger.removeCount(getApplicationContext());
        }
    }
}

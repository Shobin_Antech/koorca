package com.koorca.viewpager;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

/**
 * Created by sourav on 25/9/15.
 */
public class WelcomePagerAdapter extends FragmentPagerAdapter {

    ArrayList<Fragment> fragments;
    public WelcomePagerAdapter(FragmentManager fm) {
        super(fm);
        fragments=new ArrayList<Fragment>();
        fragments.add(new PageOne());
        fragments.add(new PageTwo());
        fragments.add(new PageThree());
        fragments.add(new PageFour());
        fragments.add(new PageFive());
        fragments.add(new PageSix());
        fragments.add(new PageSeven());

    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }
}
package com.koorca.fcm_classes;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.koorca.MainActivity;
import com.koorca.R;
import com.koorca.navigation_activities.NotificationClass;
import com.koorca.service.GetCountNotification;
import com.koorca.utilities.CommonUtility;
import com.koorca.utilities.Constants;

/**
 * Created by Belal on 5/27/2016.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    SharedPreferences sPref;


   /* @Override
    public void onCreate() {
        super.onCreate();
    }*/

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        sPref = getSharedPreferences(Constants.SHARED_PREF, MODE_PRIVATE);
        stopService(new Intent(this,GetCountNotification.class));
        startService(new Intent(this, GetCountNotification.class));
        sendNotification(remoteMessage.getData().get("title"));
         /* if (sPref.getBoolean(Constants.NOTIFICATION_STATUS, true)) {*/
       //sendNotification(remoteMessage.getData().get("body"));

     /*  } else {

        }*/


    }

    //This method is only generating push notification
    //It is same as we did in earlier posts
    private void sendNotification(String messageBody) {

        int randomPIN = (int) (Math.random() * 9000) + 1000;
        Intent intent = new Intent(this, NotificationClass.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        //
        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle("Koorca")
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(randomPIN, notificationBuilder.build());


    }
}

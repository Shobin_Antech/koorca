package com.koorca;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.provider.SyncStateContract;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.koorca.customclasses.CustomBoldText;
import com.koorca.customclasses.CustomMediumText;
import com.koorca.interfaces.ValidateClass;
import com.koorca.modal.CountryListModal;
import com.koorca.navigation_activities.UpdateNumber;
import com.koorca.utilities.CommonUtility;
import com.koorca.utilities.Constants;
import com.koorca.utilities.CustomProgressDialog;
import com.koorca.utilities.VerificationCodeAPI;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static android.R.attr.editable;


/**
 * Created by sourav on 31/8/16.
 */

public class ValidateNumber extends Activity implements View.OnClickListener {
    CustomProgressDialog pDialog;
    CustomMediumText mCountry, mCountryCode;
    CustomBoldText mContinue;
    EditText mMobileNumber;
    CommonUtility utility;
    SharedPreferences sPref;
    SharedPreferences.Editor editor;
    Gson gson;
    Type type;
    ArrayList<CountryListModal> alList;
    CharSequence[] countries;
    String sMobileNumber, sCode, sResponse;
    public static Activity fa;
    String code;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.validate_number);
        fa = this;
        pDialog = new CustomProgressDialog(this, "Please wait...", false);
        utility = new CommonUtility(this);
        sPref = getSharedPreferences(Constants.SHARED_PREF, MODE_PRIVATE);
        editor = sPref.edit();

        mCountry = (CustomMediumText) findViewById(R.id.tvCountry);
        mCountryCode = (CustomMediumText) findViewById(R.id.tvCountryCode);
        mMobileNumber = (EditText) findViewById(R.id.tvMobileNumber);
        mContinue = (CustomBoldText) findViewById(R.id.tvContinue);
        mMobileNumber.setTypeface(MyApplication.typefaceMedium);
        mCountry.setOnClickListener(this);
        mContinue.setOnClickListener(this);
        mMobileNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (mMobileNumber.getText().toString().matches("^0")) {

                    Toast.makeText(ValidateNumber.this, "Phone number having prefix 0 are not allowed", Toast.LENGTH_SHORT).show();
                    mMobileNumber.setText("");

                }
                else if (mMobileNumber.getText().toString().startsWith("0"))
                {
                    Toast.makeText(ValidateNumber.this, "Phone number having prefix 0 are not allowed", Toast.LENGTH_SHORT).show();
                    mMobileNumber.setText(mMobileNumber.getText().toString().substring(1));

                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        gson = new Gson();
        type = new TypeToken<ArrayList<CountryListModal>>() {
        }.getType();
        alList = gson.fromJson(sPref.getString(Constants.CITY_LIST, ""), type);
        alList = new ArrayList<>();
        if (alList == null || alList.size() == 0) {
            if (utility.checkInternetConection()) {
                pDialog.showDialog();
                callAPICountry();
            } else {
                showSettingsAlert();

            }

        }
    }

    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        alertDialog.setTitle("SETTINGS");

        alertDialog
                .setMessage("Please connect to internet...");
        alertDialog.setCancelable(false);

        alertDialog.setPositiveButton("Settings",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));


                    }
                });


        alertDialog.show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvCountry:
                showCountriesList();

                break;

            case R.id.tvContinue:
                if (utility.checkInternetConection()) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    sMobileNumber = mMobileNumber.getText().toString().trim();
                   sCode = mCountryCode.getText().toString().trim();


                     if (sCode.length() == 0) {
                        Toast.makeText(ValidateNumber.this, "Fill the code by select the city.", Toast.LENGTH_LONG).show();
                    }
                   else if(mCountry.getText().toString().length()==0)
                    {
                        Toast.makeText(ValidateNumber.this, "Please select the country", Toast.LENGTH_LONG).show();

                    }
                    else if (sMobileNumber.length() == 0) {
                        Toast.makeText(ValidateNumber.this, "Enter the mobile number", Toast.LENGTH_LONG).show();

                    } else if (sMobileNumber.length() < 8 || sMobileNumber.length() > 10) {
                        Toast.makeText(ValidateNumber.this, "Mobile Number should be between 8 to 10 digits.", Toast.LENGTH_LONG).show();

                    } else {
                        callAPI();
                    }

                } else {
                    Toast.makeText(ValidateNumber.this, getResources().getString(R.string.connect_to_internet), Toast.LENGTH_LONG).show();
                }

                break;
        }
    }


    private void showCountriesList() {

        countries = new CharSequence[alList.size()];
        for (int i = 0; i < alList.size(); i++) {
            CountryListModal name = alList.get(i);
            countries[i] = name.getCountryName();
        }
        AlertDialog.Builder alertdialogbuilder = new AlertDialog.Builder(ValidateNumber.this, R.style.MyDialogTheme);


        alertdialogbuilder.setTitle("Select Country ");

        alertdialogbuilder.setItems(countries, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mCountry.setText(alList.get(which).getCountryName());
                code=alList.get(which).getCountryCode();
                mCountryCode.setText(alList.get(which).getCountryCode());
                editor.putString(Constants.EMERGENCY_NUMBER, alList.get(which).getCountryPoliceNumber()).commit();


            }
        });

        AlertDialog dialog = alertdialogbuilder.create();

        dialog.show();


    }

    private void callAPI() {
        if (utility.checkInternetConection()) {
            pDialog.showDialog();
            if (code.contains("+")) {
                code = code.replace("+", "");
            }
            VerificationCodeAPI.getOTP(code + sMobileNumber, new ValidateClass() {
                @Override
                public void returnMessage(String message) {
                    editor.putString(Constants.MOBILE_NUMBER, code + sMobileNumber).commit();
                    editor.putString(Constants.COUNTRY_CODE, code).commit();
                    editor.putString(Constants.USER_NUMBER, sMobileNumber).commit();
                    editor.putString(Constants.SELECTED_COUNTRY, mCountry.getText().toString()).commit();

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            editor.putBoolean(Constants.UPDATE_NUMBER, false).commit();
                            Intent i = new Intent(ValidateNumber.this, VerificationCode.class);
                            startActivity(i);
                            overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                            pDialog.hideDialog();

                        }
                    }, 1000);


                }
            });

        } else {
            pDialog.hideDialog();

            Toast.makeText(this, getResources().getString(R.string.connect_to_internet), Toast.LENGTH_LONG).show();
        }

    }

    private void callAPICountry() {
        String url = "http://koorca.s4.staging-host.com/KOORCA/GetCountryList";


        StringRequest strReq = new StringRequest(Request.Method.GET,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("Result", response.toString());
                try {
                    saveCountryList(response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("error", "Error: " + error.getMessage());
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Accept", "application/json");

                return headers;
            }
        };
        MyApplication.getInstance().addToRequestQueue(strReq, "country_name");

    }

    private void saveCountryList(String response) throws JSONException {

        JSONArray jsonArray = new JSONArray(response);
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            CountryListModal code = new CountryListModal();
            code.setCountryId(jsonObject.getString("CountryId"));
            code.setCountryName(jsonObject.getString("CountryName"));
            code.setCountryCode(jsonObject.getString("CountryCode"));
            code.setCountryPoliceNumber(jsonObject.getString("CountryPoliceCode"));
            alList.add(code);
        }
        pDialog.hideDialog();
    }
}


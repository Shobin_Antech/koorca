package com.koorca.modal;

/**
 * Created by sourav on 31/8/16.
 */

public class CountryListModal {
    String countryId;
    String countryName;
    String countryCode;
    String countryPoliceNumber;

    public String getCountryPoliceNumber() {
        return countryPoliceNumber;
    }

    public void setCountryPoliceNumber(String countryPoliceNumber) {
        this.countryPoliceNumber = countryPoliceNumber;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }
}

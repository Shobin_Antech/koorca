package com.koorca;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import com.koorca.modal.ImageListModal;
import com.koorca.service.UploadImagesToServer;

import com.koorca.utilities.CommonUtility;
import com.koorca.utilities.Constants;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by sourav on 27/9/16.
 */

public class SyncClass extends AsyncTask<Void, Void, Void> {
    File imageFile;
    Context c;
    byte[] da;
    SharedPreferences sPref;
    SharedPreferences.Editor editor;
    private CommonUtility commonUtility;
    String date;
    String utc, zone;
    private Timer mTimer1;
    private TimerTask mTt1;
    private Handler mTimerHandler = new Handler();
    int rotation_image;

    public SyncClass(Context con, byte[] data, String date, String utc, String zone,int rot) {
        commonUtility = new CommonUtility(con);
        this.c = con;
        this.da = data;
        this.date = date;
        this.utc = utc;
        this.zone = zone;
        rotation_image=rot;
        sPref = c.getSharedPreferences(Constants.SHARED_PREF, Context.MODE_PRIVATE);
        editor = sPref.edit();

    }

    @Override
    protected Void doInBackground(Void... voids) {
        Bitmap bitmapPicture = BitmapFactory.decodeByteArray(da, 0, da.length);

//        Matrix matrix = new Matrix();
//        matrix.postRotate(90);
//        Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmapPicture, (int) (bitmapPicture.getWidth() / 2.50), (int) (bitmapPicture.getHeight() / 2.50), true);
//        Bitmap rotatedBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
        FileOutputStream outStream = null;
        Calendar c = Calendar.getInstance();
        File folder = new File(Environment
                .getExternalStorageDirectory() + "/.Koorca");
        if (!folder.exists()) {
            folder.mkdirs();

        }
        boolean successValue = true;

        if (successValue) {
            java.util.Date date = new java.util.Date();
            imageFile = new File(folder.getAbsolutePath()
                    + File.separator
                    + System.currentTimeMillis()
                    + "Image.jpg");

            try {
                imageFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {

        }

        try {
            // Write to SD Card
            outStream = new FileOutputStream(imageFile);
            outStream.write(da);
            outStream.flush();
            outStream.close();


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {

        }
        int rotate=0;
        int orientation=0;

        switch (rotation_image) {
            case 1:
                rotate = 180;
                break;
            case 2:
                rotate = -90;


                break;
            case 3:

                rotate = 0;

                break;
            case 0:
                rotate = 90;


                break;
        }
        String photopath = imageFile.toString();
        Bitmap bmp = BitmapFactory.decodeFile(photopath);

        Matrix matrix = new Matrix();
        matrix.postRotate(rotate);
        //  bmp = Bitmap.createBitmap(bmp, 0, 0,bmp.getWidth(),bmp.getHeight(), matrix, true);

        Bitmap scaledBitmap = Bitmap.createScaledBitmap(bmp, (int) (bmp.getWidth() / 2), (int) (bmp.getHeight() / 2), true);
        bmp = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);


        FileOutputStream fOut;
        try {
            fOut = new FileOutputStream(imageFile);
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
            fOut.flush();
            fOut.close();

        } catch (FileNotFoundException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        new UploadImage().execute();

    }

    class UploadImage extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... strings) {

            try {
                saveImagesInArrayList();
            } catch (ParseException e) {
                e.printStackTrace();
            }

            return null;
        }

        private void saveImagesInArrayList() throws ParseException {

                Gson gson1 = new Gson();
                Type listType = new TypeToken<ArrayList<ImageListModal>>() {
                }.getType();
                ArrayList<ImageListModal> alImages = gson1.fromJson(sPref.getString(Constants.IMAGES_LIST, ""), listType);


                ImageListModal images = new ImageListModal();
                images.setImagestring(imageFile.toString());
                images.setImagename(imageFile.getName());
                images.setCreated_date(date);
                images.setUtcTime(utc);
                images.setZone(zone);
                images.setLat(sPref.getString(Constants.LATITUDE, ""));
                images.setLng(sPref.getString(Constants.LONGITUDE, ""));


                if (alImages == null) {
                    alImages = new ArrayList<>();
                }
                alImages.add(images);
                Gson gson = new Gson();
                String image_list = gson.toJson(alImages);
                editor.putString(Constants.IMAGES_LIST, image_list).commit();
                Log.e("Images in MainClass", alImages.size() + "");

                if (commonUtility.checkInternetConection()) {
                   // if (!commonUtility.isMyServiceRunning(UploadImagesToServer.class)) {
                    Intent intents = new Intent(c, UploadImagesToServer.class);
                    c.startService(intents);
                  //    }
                }



        }

    }
}

package com.koorca;

import android.app.Application;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.koorca.navigation_activities.MainClass;
import com.koorca.utilities.Constants;

import org.jsoup.Jsoup;

import java.io.IOException;

import me.leolin.shortcutbadger.ShortcutBadger;

/**
 * Created by sourav on 30/8/16.
 */

public class MyApplication extends Application {
    public static Typeface typefaceLight, typefaceRegular, typefaceMedium, typefaceBold;
    private RequestQueue mRequestQueue;
    public static final String TAG = MyApplication.class
            .getSimpleName();
    private static MyApplication mInstance;
    SharedPreferences sPref;
    SharedPreferences.Editor editor;

    @Override
    public void onCreate() {
        super.onCreate();


        mInstance = this;
        typefaceLight = Typeface.createFromAsset(getAssets(), "font/ubuntu_light.ttf");
        typefaceRegular = Typeface.createFromAsset(getAssets(), "font/ubuntu_bold.ttf");
        typefaceMedium = Typeface.createFromAsset(getAssets(), "font/ubuntu_medium.ttf");
        typefaceBold = Typeface.createFromAsset(getAssets(), "font/ubuntu_bold.ttf");
        sPref = getSharedPreferences(Constants.SHARED_PREF, MODE_PRIVATE);
        editor = sPref.edit();

        //new versionChecker().execute();

    }

    class versionChecker extends AsyncTask<Void, Void, Void> {
        String versionName = BuildConfig.VERSION_NAME;
        String newVersion;

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                newVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=com.koorca&hl=en")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get()
                        .select("div[itemprop=softwareVersion]")
                        .first()
                        .ownText();
            } catch (IOException e) {
                e.printStackTrace();
                //Toast.makeText(MyApplication.this,"Appp",Toast.LENGTH_SHORT).show();

            }
            if (versionName.equals(newVersion)) {
                editor.putBoolean(Constants.NEW_VERSON, false).commit();
            } else {
                editor.putBoolean(Constants.NEW_VERSON, true).commit();


            }
            return null;
        }
    }
   /* private void checkVersion() throws PackageManager.NameNotFoundException {
        PackageInfo pInfo =getPackageManager().getPackageInfo(getPackageName(), 0);
        String version = pInfo.versionName;


    }*/

    public static MyApplication getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }


    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }
}

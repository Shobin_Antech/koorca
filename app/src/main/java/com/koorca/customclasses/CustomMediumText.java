package com.koorca.customclasses;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by parikshit_shekhawat on 6/6/16.
 */
public class CustomMediumText extends TextView {
    private static final String TAG = "TextView";

    public CustomMediumText(Context context) {
        super(context);
        Typeface face= Typeface.createFromAsset(context.getAssets(), "font/ubuntu_medium.ttf");
        this.setTypeface(face);
    }

    public CustomMediumText(Context context, AttributeSet attrs) {
        super(context, attrs);
        Typeface face= Typeface.createFromAsset(context.getAssets(), "font/ubuntu_medium.ttf");
        this.setTypeface(face);
    }

    public CustomMediumText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        Typeface face= Typeface.createFromAsset(context.getAssets(),"font/ubuntu_medium.ttf");
        this.setTypeface(face);
    }

    protected void onDraw (Canvas canvas) {
        super.onDraw(canvas);


    }
}
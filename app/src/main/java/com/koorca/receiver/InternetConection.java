package com.koorca.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.koorca.service.CountryName;



import com.koorca.service.UploadImagesToServer;

import com.koorca.utilities.CommonUtility;

/**
 * Created by sourav on 31/8/16.
 */

public class InternetConection extends BroadcastReceiver {
    CommonUtility utility;

    @Override
    public void onReceive(Context context, Intent intent) {
        utility = new CommonUtility(context);
        if (utility.checkInternetConection()) {
            context.startService(new Intent(context, CountryName.class));
            Log.e("Internet On","Iternet Called");
            Intent intents = new Intent(context, UploadImagesToServer.class);
            context.startService(intents);

        } else {
            Log.e("Internet Off","Iternet Off");
             Toast.makeText(context, "Please connect to internet", Toast.LENGTH_SHORT).show();
        }


    }
}

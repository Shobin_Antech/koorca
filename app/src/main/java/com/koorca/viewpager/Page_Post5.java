package com.koorca.viewpager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;


import com.koorca.MainActivity;
import com.koorca.R;
import com.koorca.customclasses.CustomRegularText;
import com.koorca.navigation_activities.MainClass;
import com.koorca.utilities.CommonUtility;
import com.koorca.utilities.Constants;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by sourav on 27/9/16.
 */

public class Page_Post5 extends Fragment {
    CustomRegularText mDone, mLink;
    private boolean result;
    private CommonUtility utility;
    View view;
    SharedPreferences sPref;
    SharedPreferences.Editor editor;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.post_page5, null);
        utility = new CommonUtility(getActivity());
        sPref = getActivity().getSharedPreferences(Constants.SHARED_PREF, MODE_PRIVATE);
        editor = sPref.edit();
        mDone = (CustomRegularText) view.findViewById(R.id.tvDone);
        mLink = (CustomRegularText) view.findViewById(R.id.textLastPage);
        SpannableString ss = new SpannableString(getResources().getString(R.string.last_page));
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                Toast.makeText(getActivity(), "Clicked", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };
        ss.setSpan(clickableSpan, 125, 144, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        mLink.setText(ss);
        mLink.setMovementMethod(LinkMovementMethod.getInstance());
        mLink.setHighlightColor(Color.TRANSPARENT);

        mLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        mDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editor.putBoolean(Constants.TUTORIAL_FINAL, true).commit();
                Intent iMain = new Intent(getActivity(), MainClass.class);
                startActivity(iMain);
                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                getActivity().finish();
            }
        });


        return view;
    }

}
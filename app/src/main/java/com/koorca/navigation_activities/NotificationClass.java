package com.koorca.navigation_activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Movie;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.koorca.MainActivity;
import com.koorca.MyApplication;
import com.koorca.R;
import com.koorca.adapters.NotificationAdapter;
import com.koorca.customclasses.CustomBoldText;
import com.koorca.modal.NotificationModal;
import com.koorca.utilities.CommonUtility;
import com.koorca.utilities.Constants;
import com.koorca.utilities.CustomProgressDialog;
import com.tubb.smrv.SwipeMenuRecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by sourav on 10/11/16.
 */

public class NotificationClass extends ActivityToolbar implements NotificationAdapter.ChangeCount, NotificationAdapter.NoNotificationAvailable {
    SharedPreferences sPref;
    SharedPreferences.Editor editor;
    CommonUtility utility;
    SwipeMenuRecyclerView recycler;
    NotificationAdapter adapter;
    RecyclerView.LayoutManager manager;
    int notificationCount;
    ArrayList<NotificationModal> al = new ArrayList<>();
    private Paint p = new Paint();
    CustomProgressDialog dialog;
    RelativeLayout topLL;
    int n_count;
    CustomBoldText mNoNotification;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout frameLayout = (FrameLayout) findViewById(R.id.frameLayouts);
        frameLayout.addView(getLayoutInflater().inflate(R.layout.notification_class, null));
        mName.setText("Notifications");
        mSettings.setVisibility(View.VISIBLE);
        mNotificationCount.setVisibility(View.VISIBLE);
        mBellIconNotification.setVisibility(View.VISIBLE);
        dialog = new CustomProgressDialog(this, "Please wait...", false);
        utility = new CommonUtility(this);
        sPref = getSharedPreferences(Constants.SHARED_PREF, MODE_PRIVATE);
        editor = sPref.edit();
        recycler = (SwipeMenuRecyclerView) findViewById(R.id.notificationRecycler);
        topLL = (RelativeLayout) findViewById(R.id.topLL);
        mNoNotification = (CustomBoldText) findViewById(R.id.tvNoNotification);
        manager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recycler.setLayoutManager(manager);
        adapter = new NotificationAdapter(this, al, this, this);
        recycler.setAdapter(adapter);

        n_count = Integer.parseInt(sPref.getString(Constants.NOTIFICATION_COUNT, "0"));

        if (utility.checkInternetConection())
            callAPI();

        else
            Snackbar.make(topLL, getResources().getString(R.string.connect_to_internet), Snackbar.LENGTH_SHORT).show();


    }


    public void callAPI() {
        dialog.showDialog();
        String url = "http://koorca.s4.staging-host.com/KOORCA/GetMessageList?deviceId=" + sPref.getString(Constants.DEVICE_ID, "");


        StringRequest strReq = new StringRequest(Request.Method.GET,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("Result", response.toString());
                try {
                    parseResponse(response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("error", "Error: " + error.getMessage());
                dialog.hideDialog();
            }
        }) {


        };
        MyApplication.getInstance().addToRequestQueue(strReq, "country_name");


    }

    private void parseResponse(String response) throws JSONException {
        JSONObject jsonObject = new JSONObject(response);
        al.clear();
        if (jsonObject.getBoolean("status")) {
            JSONArray jsonArray = jsonObject.getJSONArray("Obj");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                NotificationModal modal = new NotificationModal();
                modal.setMessageId(jsonObject1.getString("MessageId"));
                modal.setTitle(jsonObject1.getString("Title"));
                modal.setMessage(jsonObject1.getString("Message"));
                modal.setRead(jsonObject1.getBoolean("IsRead"));
                modal.setTime(jsonObject1.getString("Datetime"));
                al.add(modal);

            }
            dialog.hideDialog();

            if (al.size() == 0) {
                editor.putString(Constants.NOTIFICATION_COUNT,"0").commit();

                mNotificationCount.setVisibility(View.INVISIBLE);
                mBellIconNotification.setVisibility(View.VISIBLE);
                recycler.setVisibility(View.GONE);
                mNoNotification.setVisibility(View.VISIBLE);
            } else {
                recycler.setVisibility(View.VISIBLE);
                mNoNotification.setVisibility(View.GONE);
            }
            adapter.notifyDataSetChanged();
        }
    }

   /* public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private NotificationClass.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final NotificationClass.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }*/

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (sPref.getString(Constants.NOTIFICATION_COUNT, "0").equals("0")) {
            mNotificationCount.setVisibility(View.INVISIBLE);
            mBellIconNotification.setVisibility(View.VISIBLE);

        } else {
            mNotificationCount.setVisibility(View.VISIBLE);
            mBellIconNotification.setVisibility(View.VISIBLE);
            mNotificationCount.setText(sPref.getString(Constants.NOTIFICATION_COUNT, "0"));

        }
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter("com.koorca.notification_come"));
    }

    @Override
    public void countNotification() {
        n_count = Integer.parseInt(sPref.getString(Constants.NOTIFICATION_COUNT, "0"));

        n_count = n_count - 1;
        if (n_count < 0) {

        } else {
            editor.putString(Constants.NOTIFICATION_COUNT, n_count + "").commit();

        }
        if (n_count == 0 || n_count < 0) {
            mNotificationCount.setVisibility(View.INVISIBLE);
            mBellIconNotification.setVisibility(View.VISIBLE);

        } else {
            mNotificationCount.setVisibility(View.VISIBLE);
            mBellIconNotification.setVisibility(View.VISIBLE);
            mNotificationCount.setText(n_count + "");

        }
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String message = intent.getStringExtra("notification_count");
            mNotificationCount.setVisibility(View.VISIBLE);
            mNotificationCount.setText(message);

            callAPI();
            Log.d("receiver", "Got message: " + message);
        }
    };

    @Override
    public void noNotification() {
        recycler.setVisibility(View.GONE);
        mNoNotification.setVisibility(View.VISIBLE);
        mBellIconNotification.setVisibility(View.VISIBLE);
    }
}

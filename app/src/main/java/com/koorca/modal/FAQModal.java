package com.koorca.modal;

/**
 * Created by sourav on 29/11/16.
 */

public class FAQModal {
    String que;
    String ans;
    boolean isOpen;

    public String getQue() {
        return que;
    }

    public void setQue(String que) {
        this.que = que;
    }

    public String getAns() {
        return ans;
    }

    public void setAns(String ans) {
        this.ans = ans;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }
}

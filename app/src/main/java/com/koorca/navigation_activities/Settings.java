package com.koorca.navigation_activities;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.koorca.R;
import com.koorca.customclasses.CustomMediumText;
import com.koorca.service.LocationUpdateService;
import com.koorca.utilities.Constants;

/**
 * Created by sourav on 8/11/16.
 */

public class Settings extends ActivityToolbar {
    CustomMediumText mText;
    CheckBox mCheckBox;
    SharedPreferences sPref;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout framelayout = (FrameLayout) findViewById(R.id.frameLayouts);
        framelayout.addView(getLayoutInflater().inflate(R.layout.settings_activity,
                null));
        mName.setText("Settings");
        mSettings.setVisibility(View.GONE);
        mNotificationCount.setVisibility(View.GONE);
        mBellIconNotification.setVisibility(View.GONE);
        mText = (CustomMediumText) findViewById(R.id.text);
        mCheckBox = (CheckBox) findViewById(R.id.checkBox);
        sPref = getSharedPreferences(Constants.SHARED_PREF, MODE_PRIVATE);
        editor = sPref.edit();
        if (sPref.getBoolean(Constants.NOTIFICATION_STATUS,true)) {
            mCheckBox.setChecked(true);
            mText.setText("Notifications On");
        } else {
            mCheckBox.setChecked(false);
            mText.setText("Notifications Off");

        }
        mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                if (!checked) {
                    mText.setText("Notifications Off");
                    editor.putBoolean(Constants.NOTIFICATION_STATUS,false).commit();
                    stopService(new Intent(Settings.this, LocationUpdateService.class));

                    startService(new Intent(Settings.this, LocationUpdateService.class));

                } else {
                    mText.setText("Notifications On");
                    editor.putBoolean(Constants.NOTIFICATION_STATUS,true).commit();
                    stopService(new Intent(Settings.this, LocationUpdateService.class));
                    startService(new Intent(Settings.this, LocationUpdateService.class));


                }

            }
        });
    }


}

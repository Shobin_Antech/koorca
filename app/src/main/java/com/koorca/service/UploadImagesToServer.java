package com.koorca.service;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.koorca.MyApplication;
import com.koorca.R;
import com.koorca.modal.ImageListModal;
import com.koorca.utilities.CommonUtility;
import com.koorca.utilities.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static android.R.id.message;

/**
 * Created by sourav on 2/9/16.
 */

public class UploadImagesToServer extends Service {
    SharedPreferences sPref;
    // Type listType;
    Context ctx = this;
    String imageString, imageName, createdDate;
    private SharedPreferences.Editor editor;
    CommonUtility utility;
    Intent broadcastIntent;
    String imageStringbase;
    ActivityManager am;
    List<ActivityManager.RunningTaskInfo> taskInfo;
    ComponentName componentInfo;
    String act_name;
    NotificationManager notificationManager;
    String utc,zone,lat,lng;

    @Override
    public void onCreate() {
        super.onCreate();
        sPref = getSharedPreferences(Constants.SHARED_PREF, MODE_PRIVATE);
        editor = sPref.edit();
        utility = new CommonUtility(this);
        broadcastIntent = new Intent();
        broadcastIntent.setAction("response");


        getImagesFromSharedPref();


    }

    private void getImagesFromSharedPref() {
        if (sPref.getString(Constants.LONGITUDE, "").equals("")) {
            startService(new Intent(this, FusedLocationService.class));
        }

        if (utility.checkInternetConection()) {
            ArrayList<ImageListModal> alImages = new ArrayList<>();
            Gson gson = new Gson();
            Type listType = new TypeToken<ArrayList<ImageListModal>>() {
            }.getType();
            alImages = gson.fromJson(sPref.getString(Constants.IMAGES_LIST, ""), listType);
            if (alImages == null || alImages.size() == 0) {
                Log.e("Error", "Nothing to share");
                stopSelf();
                onDestroy();

            } else {

                new AsyncClass().execute(alImages);
                /*ImageListModal modal = alImages.get(0);

                imageString = modal.getImagestring();
                Bitmap bm = BitmapFactory.decodeFile(imageString);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
                byte[] b = baos.toByteArray();
                String imageString = Base64.encodeToString(b, Base64.DEFAULT);
                createdDate = modal.getCreated_date();
                imageName = modal.getImagename();

                uploadImages(imageName, imageString, createdDate);*/
            }
        } else {
            Toast.makeText(UploadImagesToServer.this, "Please connect to Internet", Toast.LENGTH_SHORT).show();
        }
    }


    private void uploadImages(final String image_name, final String image_string, final String created_date, final String utc, final String zone, final String lat, final String lng) {
        String url = "http://koorca.s4.staging-host.com/KOORCA/post";


        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("Result", response.toString());


                try {
                    parseResponse(response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("error", "Error: " + error.getMessage());
                am = (ActivityManager) UploadImagesToServer.this.getSystemService(Context.ACTIVITY_SERVICE);
                taskInfo = am.getRunningTasks(1);
                componentInfo = taskInfo.get(0).topActivity;
                //Log.d("GETTIN CURRENT ACTIICY AT TOPO", "CURRENT Activity ::" + taskInfo.get(0).topActivity.getClassName()+"   Package Name :  "+componentInfo.getPackageName());
                act_name = taskInfo.get(0).topActivity.getClassName();
                if (act_name.equals("com.koorca.navigation_activities.MainClass")) {
                    broadcastIntent.putExtra("responses", "Not uploaded");
                    sendBroadcast(broadcastIntent);
                } else {
                    NotificationCompat.Builder builder = new NotificationCompat.Builder(UploadImagesToServer.this);

                    Notification notification = builder.setContentTitle("Koorca")
                            .setContentText("Not uploaded")
                            .setSmallIcon(R.drawable.ic_launcher)
                            .build();

                    notificationManager = (NotificationManager) UploadImagesToServer.this.getSystemService(Context.NOTIFICATION_SERVICE);
                    notificationManager.notify(0, notification);
                    hideNotification();
                }


                getImagesFromSharedPref();


            }
        }) {

            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put("Lat",lat);
                params.put("Long",lng);
                /*params.put("Lat", "-32.645746");
                params.put("Long", "134.917298");*/


                params.put("City", "");
                params.put("State", "");
                params.put("Country", "");
                params.put(" FullAddress", "");
                params.put("ImageBase64", image_string);
                params.put("ImageName", image_name);
                params.put("CreatedDate", created_date);
                params.put("Createdby", "");
                params.put("PhoneNumber", sPref.getString(Constants.MOBILE_NUMBER, ""));
                params.put("PhoneId", sPref.getString(Constants.PHONE_ID, ""));
                params.put(" CreatedDateUTC", utc);
                params.put("TimeZone",zone);


                return params;
            }

        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(strReq, "profile_Pic");
    }


    private void parseResponse(String response) throws JSONException {
        JSONObject jsonObject = new JSONObject(response);
        if (jsonObject.getBoolean("status")) {
            Log.e("Response", jsonObject.getString("message"));
            Gson gson = new Gson();
            Type listType = new TypeToken<ArrayList<ImageListModal>>() {
            }.getType();
            ArrayList<ImageListModal> alImages = gson.fromJson(sPref.getString(Constants.IMAGES_LIST, ""), listType);

            File file = new File(imageString);
            boolean deleted = file.delete();
            alImages.remove(0);


            String image_list = gson.toJson(alImages);
            editor.putString(Constants.IMAGES_LIST, image_list).commit();

        } else {

        }
        try {
            am = (ActivityManager) this.getSystemService(Context.ACTIVITY_SERVICE);
            taskInfo = am.getRunningTasks(1);
            componentInfo = taskInfo.get(0).topActivity;
            //Log.d("GETTIN CURRENT ACTIICY AT TOPO", "CURRENT Activity ::" + taskInfo.get(0).topActivity.getClassName()+"   Package Name :  "+componentInfo.getPackageName());
            act_name = taskInfo.get(0).topActivity.getClassName();
            if (act_name.equals("com.koorca.navigation_activities.MainClass")) {
                broadcastIntent.putExtra("responses", jsonObject.getString("message"));
                sendBroadcast(broadcastIntent);
            } else {
                NotificationCompat.Builder builder = new NotificationCompat.Builder(this);

                Notification notification = builder.setContentTitle("Koorca")
                        .setContentText(jsonObject.getString("message"))
                        .setSmallIcon(getNotificationIcon())
                        .build();

                notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.notify(0, notification);
                hideNotification();
            }

            getImagesFromSharedPref();

        } catch (Exception e) {
            Log.e("Exceptionption", e.getMessage());
        }

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    class AsyncClass extends AsyncTask<ArrayList<ImageListModal>, Void, Void> {


        @Override
        protected Void doInBackground(ArrayList<ImageListModal>... arrayLists) {
            try {
                ArrayList<ImageListModal> passed = arrayLists[0]; //get passed arraylist
                ImageListModal modal = passed.get(0);
                imageString = modal.getImagestring();
                Bitmap bm = BitmapFactory.decodeFile(imageString);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
                byte[] b = baos.toByteArray();
                imageStringbase = Base64.encodeToString(b, Base64.DEFAULT);
                createdDate = modal.getCreated_date();
                imageName = modal.getImagename();
                utc=modal.getUtcTime();
                zone=modal.getZone();
                lat=modal.getLat();
                lng=modal.getLng();
            } catch (Exception e) {
                stopSelf();
                startService(new Intent(ctx, UploadImagesToServer.class));
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            uploadImages(imageName, imageStringbase, createdDate,utc,zone,lat,lng);

        }
    }

    private void hideNotification() {
        Handler h = new Handler();

        h.postDelayed(new Runnable() {
            public void run() {
                notificationManager.cancel(0);
            }
        }, 2000);
    }

    private int getNotificationIcon() {
        boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.drawable.ic_launcher : R.drawable.ic_launcher;
    }

}

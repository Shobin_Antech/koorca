package com.koorca.service;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.koorca.MyApplication;
import com.koorca.modal.CountryListModal;
import com.koorca.utilities.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;



/**
 * Created by sourav on 31/8/16.
 */

public class CountryName extends Service {
    ArrayList<CountryListModal> alName = new ArrayList<>();
    SharedPreferences sPref;
    SharedPreferences.Editor editor;

    @Override
    public void onCreate() {
        super.onCreate();
        sPref = getSharedPreferences(Constants.SHARED_PREF, MODE_PRIVATE);
        editor = sPref.edit();
        callAPI();

    }

    private void callAPI() {

            String url = "http://koorca.s4.staging-host.com/KOORCA/GetCountryList";


            StringRequest strReq = new StringRequest(Request.Method.GET,
                    url, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
                    Log.d("Result", response.toString());
                    try {
                        saveCountryList(response);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d("error", "Error: " + error.getMessage());
                }
            }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    headers.put("Accept", "application/json");

                    return headers;
                }
            };
            MyApplication.getInstance().addToRequestQueue(strReq, "country_name");


    }

    private void saveCountryList(String response) throws JSONException {
        JSONArray jsonArray = new JSONArray(response);
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            CountryListModal code = new CountryListModal();
            code.setCountryId(jsonObject.getString("CountryId"));
            code.setCountryName(jsonObject.getString("CountryName"));
            code.setCountryCode(jsonObject.getString("CountryCode"));
            code.setCountryPoliceNumber(jsonObject.getString("CountryPoliceCode"));
            alName.add(code);
        }
        Gson gson = new Gson();
        String listCountry = gson.toJson(alName);
        editor.putString(Constants.CITY_LIST, listCountry).commit();

    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}

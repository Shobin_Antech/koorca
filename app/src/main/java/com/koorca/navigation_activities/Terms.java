package com.koorca.navigation_activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;

import com.koorca.R;

/**
 * Created by sourav on 10/11/16.
 */

public class Terms extends ActivityToolbar {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout framelayout = (FrameLayout) findViewById(R.id.frameLayouts);
        framelayout.addView(getLayoutInflater().inflate(R.layout.user_guide,
                null));
        mName.setText("Terms & Conditions");
        mSettings.setVisibility(View.GONE);
        mNotificationCount.setVisibility(View.GONE);
        mBellIconNotification.setVisibility(View.GONE);    }
}

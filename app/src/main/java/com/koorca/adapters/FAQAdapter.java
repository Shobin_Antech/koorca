package com.koorca.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.koorca.R;
import com.koorca.customclasses.CustomRegularText;
import com.koorca.modal.FAQModal;

import java.util.ArrayList;

/**
 * Created by sourav on 29/11/16.
 */

public class FAQAdapter extends RecyclerView.Adapter<FAQAdapter.MyHolderView> {
    Context ctx;
    ArrayList<FAQModal> al;
    FAQ faq;

    public FAQAdapter(Context ctx, ArrayList<FAQModal> al,FAQ faq) {
        this.ctx = ctx;
        this.al = al;
        this.faq=faq;
    }

    @Override
    public FAQAdapter.MyHolderView onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.faq_item, null);

        return new MyHolderView(view);
    }

    @Override
    public void onBindViewHolder(final FAQAdapter.MyHolderView holder, final int position) {
        holder.question.setText(al.get(position).getQue());
        holder.answer.setText(al.get(position).getAns());
        holder.plus.setVisibility(View.VISIBLE);
        holder.question.setTextColor(ctx.getResources().getColor(R.color.colors));

        if (al.get(position).isOpen()) {
            holder.plus.setVisibility(View.GONE);
            holder.minus.setVisibility(View.VISIBLE);
            holder.question.setVisibility(View.VISIBLE);
            holder.answer.setVisibility(View.VISIBLE);
            holder.question.setMaxLines(Integer.MAX_VALUE);
            holder.answer.setMaxLines(Integer.MAX_VALUE);
            holder.question.setText(al.get(position).getQue());
            holder.answer.setText(al.get(position).getAns());
            holder.question.setTextColor(ctx.getResources().getColor(R.color.black));
        } else {
            holder.plus.setVisibility(View.VISIBLE);
            holder.minus.setVisibility(View.GONE);
            holder.question.setVisibility(View.VISIBLE);
            holder.answer.setVisibility(View.GONE);
            holder.question.setMaxLines(1);
            holder.question.setText(al.get(position).getQue());
            holder.question.setTextColor(ctx.getResources().getColor(R.color.colors));

        }
        holder.rl1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                for(int i=0;i<al.size();i++)
                {
                    al.get(i).setOpen(false);
                }
                holder.plus.setVisibility(View.GONE);
                holder.minus.setVisibility(View.VISIBLE);
                holder.question.setVisibility(View.VISIBLE);
                holder.answer.setVisibility(View.VISIBLE);
                holder.question.setMaxLines(Integer.MAX_VALUE);
                holder.answer.setMaxLines(Integer.MAX_VALUE);
                holder.question.setText(al.get(position).getQue());
                holder.answer.setText(al.get(position).getAns());
                holder.question.setTextColor(ctx.getResources().getColor(R.color.black));
                al.get(position).setOpen(true);
                notifyDataSetChanged();

            }
        });
        holder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.plus.setVisibility(View.VISIBLE);
                holder.minus.setVisibility(View.GONE);
                holder.answer.setVisibility(View.GONE);
                holder.question.setVisibility(View.VISIBLE);
                holder.question.setMaxLines(1);
                holder.question.setText(al.get(position).getQue());
                al.get(position).setOpen(false);
                holder.question.setTextColor(ctx.getResources().getColor(R.color.colors));


            }
        });


    }

    @Override
    public int getItemCount() {
        return al.size();
    }

    public class MyHolderView extends RecyclerView.ViewHolder {
        CustomRegularText question, answer;
        ImageView plus, minus;
        RelativeLayout rl1;

        public MyHolderView(View itemView) {
            super(itemView);
            rl1=(RelativeLayout)itemView.findViewById(R.id.rl1);
            question = (CustomRegularText) itemView.findViewById(R.id.question);
            answer = (CustomRegularText) itemView.findViewById(R.id.answer);
            plus = (ImageView) itemView.findViewById(R.id.plus);
            minus = (ImageView) itemView.findViewById(R.id.minus);
        }
    }

    public interface FAQ {
        public void selected();
    }
}

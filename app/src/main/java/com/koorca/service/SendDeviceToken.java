package com.koorca.service;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.koorca.MyApplication;
import com.koorca.R;
import com.koorca.utilities.Constants;

import org.json.JSONException;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by sourav on 11/11/16.
 */

public class SendDeviceToken extends Service {
    SharedPreferences sPref;
    SharedPreferences.Editor editor;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sPref = getSharedPreferences(Constants.SHARED_PREF, MODE_PRIVATE);
        editor = sPref.edit();
        if (sPref.getBoolean(Constants.SENT_DEVICE_ID, false)) {

        } else {
            callAPI();
            editor.putBoolean(Constants.SENT_DEVICE_ID, true).commit();


        }


    }

    private void callAPI() {
        Log.e("API called", "API called1");
        String url = "http://koorca.s4.staging-host.com/KOORCA/SaveLocation";


        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("Result", response.toString());


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("error", "Error: " + error.getMessage());


            }
        }) {

            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put("PhoneNumber", sPref.getString(Constants.MOBILE_NUMBER, ""));
                params.put("DeviceId", sPref.getString(Constants.DEVICE_ID, ""));

                params.put("DeviceName", "android");
                params.put("Lat", sPref.getString(Constants.LATITUDE, ""));
                params.put("Lng", sPref.getString(Constants.LONGITUDE, ""));
                params.put("IsDelete", "true");


                return params;
            }

        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(strReq, "profile_Pic");


    }

}

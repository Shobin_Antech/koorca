package com.koorca.viewpager;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.koorca.R;
import com.koorca.ValidateNumber;
import com.koorca.service.CountryName;
import com.koorca.service.FusedLocationService;
import com.koorca.service.GPSTracker;
import com.koorca.utilities.CommonUtility;


public class WelcomeFragmentActivity extends FragmentActivity {
    private static final int PERMISSION_REQUEST_CODE = 2;
    ViewPager pager;
    public static LinearLayout mPoints;
    private ImageView[] dots;
    private int dotsCount;
    RelativeLayout header_Rl;
    ImageView logo_Iv;
    boolean result = false;
    CommonUtility utility;
    public static WelcomeFragmentActivity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_fragment);
        activity = this;
        utility = new CommonUtility(this);
        mPoints = (LinearLayout) findViewById(R.id.dotsLinear);
        header_Rl = (RelativeLayout) findViewById(R.id.topRL);
        logo_Iv = (ImageView) findViewById(R.id.logo);
        pager = (ViewPager) findViewById(R.id.view_pager);

        WelcomePagerAdapter pagerAdapter = new WelcomePagerAdapter(getSupportFragmentManager());
        pager.setAdapter(pagerAdapter);
        setUiPageViewController();
        callService();
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                if (position == 0) {
                    mPoints.setVisibility(View.VISIBLE);
                    header_Rl.setBackgroundResource(R.drawable.view_pager_back);
                    logo_Iv.setVisibility(View.VISIBLE);
                    for (int i = 0; i < dotsCount; i++) {
                        dots[i].setImageDrawable(getResources().getDrawable(R.drawable.unselected_dot));

                    }
                    dots[position].setImageDrawable(getResources().getDrawable(R.drawable.selected_dot));
                } else if (position == 1) {
                    mPoints.setVisibility(View.VISIBLE);
                    header_Rl.setBackgroundResource(R.drawable.view_pager_back);
                    logo_Iv.setVisibility(View.VISIBLE);

                    for (int i = 0; i < dotsCount; i++) {
                        dots[i].setImageDrawable(getResources().getDrawable(R.drawable.unselected_dot));

                    }
                    Log.e("onPageSelected:", position + "");
                    dots[position].setImageDrawable(getResources().getDrawable(R.drawable.selected_dot));
                } else if (position == 2) {
                    mPoints.setVisibility(View.GONE);
                    header_Rl.setBackgroundResource(R.drawable.button_desc);
                    logo_Iv.setVisibility(View.GONE);

                } else if (position == 3) {
                    mPoints.setVisibility(View.INVISIBLE);
                    header_Rl.setBackgroundResource(0);
                    logo_Iv.setVisibility(View.GONE);
                } else if (position == 4) {
                    mPoints.setVisibility(View.VISIBLE);
                    header_Rl.setBackgroundResource(0);
                    logo_Iv.setVisibility(View.GONE);

                    for (int i = 0; i < dotsCount; i++) {
                        dots[i].setImageDrawable(getResources().getDrawable(R.drawable.unselected_dot));

                    }
                    Log.e("onPageSelected:", position + "");
                    dots[position].setImageDrawable(getResources().getDrawable(R.drawable.selected_dot));
                } else if (position == 5) {
                    mPoints.setVisibility(View.GONE);
                    header_Rl.setBackgroundResource(0);
                    logo_Iv.setVisibility(View.GONE);

                } else if (position == 6) {
                    mPoints.setVisibility(View.GONE);
                    header_Rl.setBackgroundResource(0);
                    logo_Iv.setVisibility(View.GONE);
                    Intent i = new Intent(WelcomeFragmentActivity.this, ValidateNumber.class);
                    startActivity(i);
                    finish();
                    overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                }

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void setUiPageViewController() {

        dotsCount = 6;
        dots = new ImageView[dotsCount];

        for (int i = 0; i < dotsCount; i++) {
            dots[i] = new ImageView(this);
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.unselected_dot));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );

            params.setMargins(4, 0, 4, 0);

            mPoints.addView(dots[i], params);
        }

        dots[0].setImageDrawable(getResources().getDrawable(R.drawable.selected_dot));
    }

    private void callService() {
        if (utility.checkInternetConection()) {
            startService(new Intent(WelcomeFragmentActivity.this, CountryName.class));
        } else {
            Toast.makeText(WelcomeFragmentActivity.this, getResources().getString(R.string.connect_to_internet), Toast.LENGTH_LONG).show();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!utility.isGPSOn()) {
            checkGPS();
        } else {

            int currentAPIVersion = Build.VERSION.SDK_INT;
            if (currentAPIVersion >= Build.VERSION_CODES.M) {
                if (checkPermission()) {
                    startService(new Intent(WelcomeFragmentActivity.this, FusedLocationService.class));

                } else {
                    requestPermission();
                }
            }
            else
            {
                startService(new Intent(WelcomeFragmentActivity.this, FusedLocationService.class));

            }
        }
    }

    private void checkGPS() {

        showSettingsAlert("Network");

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private boolean checkPermission() {
        int result = ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION);


        return result == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {

                    boolean locationAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;

                    if (locationAccepted) {
                        Toast.makeText(getApplicationContext(), "Permission Granted, Now you can access storage and camera.", Toast.LENGTH_LONG).show();
                        startService(new Intent(WelcomeFragmentActivity.this, FusedLocationService.class));
                    } else {

                        Toast.makeText(getApplicationContext(), "Permission Denied, You cannot access storage and camera.", Toast.LENGTH_LONG).show();

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                            if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
                            showMessageOKCancel("You need to allow access to all the permissions",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                                                    requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE},
//                                                            PERMISSION_REQUEST_CODE);
                                                Intent intent = new Intent();
                                                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                                Uri uri = Uri.fromParts("package", WelcomeFragmentActivity.this.getPackageName(), null);
                                                intent.setData(uri);
                                                startActivity(intent);
                                                finish();
                                            }
                                        }
                                    });
                            return;
//                            }
                        }

                    }
                }


                break;
        }
    }


    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(WelcomeFragmentActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .create()
                .show();
    }

    public void showSettingsAlert(String provider) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setCancelable(false);

        alertDialog.setTitle(provider + " SETTINGS");

        alertDialog
                .setMessage(provider + " is not enabled! Want to go to settings menu?");

        alertDialog.setPositiveButton("Settings",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        Intent intent = new Intent(
                                Settings.ACTION_LOCATION_SOURCE_SETTINGS);

                        startActivity(intent);
                        dialog.cancel();
                        dialog.dismiss();

                    }
                });


        alertDialog.show();
    }
}

package com.koorca.utilities;

/**
 * Created by sourav on 31/8/16.
 */

public class Constants {
    public static String SHARED_PREF = "koorca";
    public static String CITY_LIST = "city_list";
    public static String MOBILE_NUMBER = "mobile_number";
    public static String EMERGENCY_NUMBER = "emergency_number";
    public static String LATITUDE = "latitude";
    public static String LONGITUDE = "longitude";
    public static String LAST_DATE = "last_date";
    public static String COUNT = "count";
    public static final String IMAGES_LIST = "images_list";
    public static String PHONE_ID = "phone_id";
    public static String USER_LOGIN = "user_login";

    public static String NOTIFICATION_STATUS = "notification_status";
    public static String DEVICE_ID = "device_id";

    public static String SENT_DEVICE_ID = "sent";
    public static String NOTIFICATION_COUNT="notification_count";
    public static String TERMS_CONDITIONS="terms_conditions";
    public static String TUTORIAL_FINAL="tutorial_last";
    public static String NEW_VERSON="new_version";
    public static String UPDATE_NUMBER="update_number";
    public static String COUNTRY_CODE="country_code";
    public static String USER_NUMBER="user_number";
    public static String SELECTED_COUNTRY="selected_country";



}

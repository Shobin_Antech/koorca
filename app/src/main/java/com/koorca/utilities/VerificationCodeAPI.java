package com.koorca.utilities;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.koorca.MyApplication;
import com.koorca.interfaces.ValidateClass;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by sourav on 31/8/16.
 */

public class VerificationCodeAPI {
    private static String message;

    public static void getOTP(String mobile_number, final ValidateClass interfac) {
        String url = "http://koorca.s4.staging-host.com/KOORCA/SendOPT/" + mobile_number;


        StringRequest strReq = new StringRequest(Request.Method.GET,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("Result", response.toString());
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    message = jsonObject.getString("message");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                interfac.returnMessage(message);


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("error", "Error: " + error.getMessage());

            }
        }) {


        };
        MyApplication.getInstance().addToRequestQueue(strReq, "get_otp");
    }


}

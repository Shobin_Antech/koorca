package com.koorca.navigation_activities;


import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.ImageFormat;
import android.graphics.PixelFormat;
import android.graphics.drawable.ColorDrawable;
import android.hardware.Camera;
import android.hardware.SensorManager;
import android.media.MediaActionSound;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.*;

import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.MotionEvent;
import android.view.OrientationEventListener;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.BounceInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;


import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.koorca.MainActivity;
import com.koorca.MyApplication;
import com.koorca.R;
import com.koorca.SyncClass;
import com.koorca.customclasses.CustomBoldText;
import com.koorca.customclasses.CustomRegularText;
import com.koorca.modal.ImageListModal;
import com.koorca.service.FusedLocationService;
import com.koorca.service.GPSTracker;
import com.koorca.service.UploadImagesToServer;
import com.koorca.utilities.CommonUtility;
import com.koorca.utilities.Constants;
import com.koorca.utilities.CustomProgressDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;

import me.leolin.shortcutbadger.ShortcutBadger;

import static android.content.ContentValues.TAG;

/**
 * Created by sourav on 10/11/16.
 */

public class MainClass extends MainActivity implements View.OnTouchListener,Camera.PictureCallback, SurfaceHolder.Callback, View.OnClickListener {

    public static final String EXTRA_CAMERA_DATA = "camera_data";

    private static final String KEY_IS_CAPTURING = "is_capturing";
    private static final int PERMISSION_REQUEST_CODE = 1;
    boolean isLimitExceeded = false;
    private Camera mCamera;
    private ImageView mCameraImage;
    private SurfaceView mCameraPreview;
    private ImageView mCaptureImageButton;
    private byte[] mCameraData;
    ArrayList<byte[]> mCameraDataList = new ArrayList<byte[]>();
    ArrayList<String> alPath = new ArrayList<>();
    private boolean mIsCapturing;
    File imageFile;
    int result;
    boolean imageSavefalse = true;

    LinearLayout mCall;
    private String encodedImage;
    private String image_name;
    SharedPreferences sPref;
    private SharedPreferences.Editor editor;
    private SimpleDateFormat dateFormat;
    private SimpleDateFormat dateFormat1;
    Toast toasts;
    private Date date;
    private String sendDateToserver;
    String currentDate;
    private String last_date;
    private ArrayList<ImageListModal> alImage = new ArrayList<>();
    CommonUtility commonUtility;
    Intent phoneIntent;
    private boolean results;
    private IntentFilter mIntentFilter, mIntentFilterLocation;
    private boolean canClick = true;
    CustomProgressDialog dialog;
    String utcDate;
    boolean isCalling = false;
    private String timeZone;
    ProgressDialog pDialog;
    boolean availableLocation = true;
    int rotations;

    private OrientationEventListener listener;
    private View.OnClickListener mCaptureImageButtonClickListener = new View.OnClickListener() {
        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onClick(View v) {

            if (sPref.getString(Constants.LATITUDE, "").equals("")) {

                startService(new Intent(MainClass.this, FusedLocationService.class));
                Toast.makeText(MainClass.this, "Fetching location...", Toast.LENGTH_SHORT).show();


            } else {
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                    MediaActionSound sound = new MediaActionSound();
                    sound.play(MediaActionSound.SHUTTER_CLICK);
                    ScaleAnimation fade_in = new ScaleAnimation(1.0f, 1.2f,
                            1.0f, 1.2f, Animation.RELATIVE_TO_SELF,
                            0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                    fade_in.setDuration(250);
                    fade_in.setInterpolator(new BounceInterpolator());
                    v.startAnimation(fade_in);
                    ScaleAnimation fade_out = new ScaleAnimation(1.2f, 1.0f,
                            1.2f, 1.0f, Animation.RELATIVE_TO_SELF,
                            0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                    fade_out.setDuration(250);
                    fade_out.setInterpolator(new BounceInterpolator());
                    v.startAnimation(fade_out);
                    if (isLimitExceeded) {
                        Toast.makeText(MainClass.this, "You can send a maximum of 25 shots a day.", Toast.LENGTH_SHORT).show();
                        return;

                    }

                    imageSavefalse = false;
                    captureImage();
                } else {
                    if (imageSavefalse) {
                        MediaActionSound sound = new MediaActionSound();
                        sound.play(MediaActionSound.SHUTTER_CLICK);
                        ScaleAnimation fade_in = new ScaleAnimation(1.0f, 1.2f,
                                1.0f, 1.2f, Animation.RELATIVE_TO_SELF,
                                0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                        fade_in.setDuration(250);
                        fade_in.setInterpolator(new BounceInterpolator());
                        v.startAnimation(fade_in);
                        ScaleAnimation fade_out = new ScaleAnimation(1.2f, 1.0f,
                                1.2f, 1.0f, Animation.RELATIVE_TO_SELF,
                                0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                        fade_out.setDuration(250);
                        fade_out.setInterpolator(new BounceInterpolator());
                        v.startAnimation(fade_out);
                        if (isLimitExceeded) {
                            Toast.makeText(MainClass.this, "You can send a maximum of 25 shots a day.", Toast.LENGTH_SHORT).show();
                            return;

                        }

                        imageSavefalse = false;
                        captureImage();

                    }
                }
            }


        }
    };

    private CustomRegularText mNotificationCountDrawer, mNotificationCountToolbar;
    ImageView mUpdateAvailable;
    private ImageView mBell;
    private float mDist;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout framelayout = (FrameLayout) findViewById(R.id.frameLayout);
        framelayout.addView(getLayoutInflater().inflate(R.layout.main_class,
                null));



        mNotificationCountDrawer = (CustomRegularText) findViewById(R.id.tvNotificationCountDrawer);
        mBell = (ImageView) findViewById(R.id.ivBell);
        mNotificationCountToolbar = (CustomRegularText) findViewById(R.id.tvNotificationCountToolbar);
        mUpdateAvailable = (ImageView) findViewById(R.id.ivUpdateAvailable);
        mBell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainClass.this, NotificationClass.class);
                startActivity(i);
            }
        });
        mNotificationCountToolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainClass.this, NotificationClass.class);
                startActivity(i);
            }
        });
        pDialog = new ProgressDialog(this);
        commonUtility = new CommonUtility(this);
        dialog = new CustomProgressDialog(this, "Please wait...", false);
        mIntentFilter = new IntentFilter();
        mIntentFilter.addAction("response");
        mIntentFilterLocation = new IntentFilter();
        mIntentFilterLocation.addAction("koorca.location");
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        mCameraImage = (ImageView) findViewById(R.id.camera_image_view);
        mCameraImage.setVisibility(View.INVISIBLE);
        mCameraPreview = (SurfaceView) findViewById(R.id.preview_view);
        final SurfaceHolder surfaceHolder = mCameraPreview.getHolder();
        surfaceHolder.addCallback(this);
        surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        mCaptureImageButton = (ImageView) findViewById(R.id.capture2);
        mCaptureImageButton.setOnClickListener(mCaptureImageButtonClickListener);
        mIsCapturing = true;
        mCall = (LinearLayout) findViewById(R.id.emergencyCall);

        if (!commonUtility.isGPSOn()) {
            checkGPS();
        } else {
            if (!commonUtility.isMyServiceRunning(FusedLocationService.class)) {
                startService(new Intent(MainClass.this, FusedLocationService.class));
            }
        }
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (checkPermission()) ;
            else {
                requestPermission();
            }
        }

       /* mUpdateAvailable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDialog();
            }
        });*/
        mCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (sPref.getString(Constants.LATITUDE, "").equals("")) {

                    startService(new Intent(MainClass.this, FusedLocationService.class));
                    Toast.makeText(MainClass.this, "Fetching location...", Toast.LENGTH_SHORT).show();


                } else {
                    try {


                        if (!isCalling) {
                            isCalling = true;
                            //  Toast.makeText(MainActivity.this, "Clicked", Toast.LENGTH_SHORT).show();

                            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                            if (isLimitExceeded) {
                                Toast.makeText(MainClass.this, "You can send a maximum of 25 shots a day.", Toast.LENGTH_SHORT).show();


                            } else {
                                captureImage();

                            }

                            toasts = Toast.makeText(MainClass.this, "Uploading",
                                    Toast.LENGTH_SHORT);
                            toasts.show();
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    if (ActivityCompat.checkSelfPermission(MainClass.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                        // TODO: Consider calling

                                        return;
                                    }

                                   /* Intent iCall = new Intent(Intent.ACTION_CALL);
                                    iCall.setData(Uri.parse("tel:" + sPref.getString(Constants.EMERGENCY_NUMBER, "")));
                                   // iCall.setClassName("com.android.phone", "com.android.phone.OutgoingCallBroadcaster");
                                    startActivity(iCall);*/
                                    //startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + sPref.getString(Constants.EMERGENCY_NUMBER, ""))));
                                    startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + sPref.getString(Constants.EMERGENCY_NUMBER, ""))));
                                    toasts.cancel();
                                    isCalling = false;
                                    // startActivity(new Intent(ACTION_CALL, Uri.parse("tel:" + "+919915397035")));
//                            finish();

                                }
                            }, 2000);
                        }


                    } catch (Exception e) {
                        Log.e("Error", e.getMessage());
                    } finally {
                        imageSavefalse = true;
                    }
                }


            }
        });

        sPref = getSharedPreferences(Constants.SHARED_PREF, MODE_PRIVATE);
        editor = sPref.edit();
        dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        dateFormat1 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        date = new Date();
        currentDate = dateFormat.format(date);
        /*if (sPref.getBoolean(Constants.NEW_VERSON, false)) {
            openDialog();
        }*/
        mCameraPreview.setOnTouchListener(this);


    }

    private void openDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.update_app);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        CustomBoldText update = (CustomBoldText) dialog.findViewById(R.id.tvUpdate);
        ImageView ivClose = (ImageView) dialog.findViewById(R.id.ivClose);
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=antier.com.gurbaniapp")));

                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=antier.com.gurbaniapp")));

                }

            }
        });
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    private void checkGPS() {

        showSettingsAlert("Network");

    }

    public void showSettingsAlert(String provider) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        alertDialog.setTitle(provider + " SETTINGS");

        alertDialog
                .setMessage(provider + " is not enabled! Want to go to settings menu?");

        alertDialog.setPositiveButton("Settings",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        Intent intent = new Intent(
                                android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);

                        startActivity(intent);
                        dialog.cancel();
                        dialog.dismiss();

                    }
                });


        alertDialog.show();
    }

    private boolean checkPermission() {
        int result = ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA);
        int result1 = ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE);
        int result2 = ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CALL_PHONE);

        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED && result2 == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CALL_PHONE}, PERMISSION_REQUEST_CODE);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {

                    boolean locationAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean readaccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean writeaccepted = grantResults[2] == PackageManager.PERMISSION_GRANTED;

                    if (locationAccepted && readaccepted && writeaccepted)
                        Toast.makeText(getApplicationContext(), "Permission Granted, Now you can access storage and camera.", Toast.LENGTH_LONG).show();
                    else {

                        Toast.makeText(getApplicationContext(), "Permission Denied, You cannot access storage and camera.", Toast.LENGTH_LONG).show();

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                            if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
                            showMessageOKCancel("You need to allow access to all the permissions",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                                                    requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE},
//                                                            PERMISSION_REQUEST_CODE);
                                                Intent intent = new Intent();
                                                intent.setAction(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                                Uri uri = Uri.fromParts("package", MainClass.this.getPackageName(), null);
                                                intent.setData(uri);
                                                startActivity(intent);
                                                finish();
                                            }
                                        }
                                    });
                            return;
//                            }
                        }

                    }
                }


                break;
        }
    }


    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(MainClass.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .create()
                .show();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(commonUtility.checkInternetConection())
        {
            startService(new Intent(MainClass.this, UploadImagesToServer.class));
        }
        /*if (sPref.getBoolean(Constants.NEW_VERSON, false)) {
            mNotificationCountToolbar.setVisibility(View.GONE);
            mBell.setVisibility(View.GONE);
            mUpdateAvailable.setVisibility(View.VISIBLE);
            if (sPref.getString(Constants.NOTIFICATION_COUNT, "0").equals("0")) {
                mNotificationCountDrawer.setVisibility(View.GONE);


            } else {
                mNotificationCountDrawer.setText(sPref.getString(Constants.NOTIFICATION_COUNT, "0"));

            }
        } */ if (sPref.getString(Constants.NOTIFICATION_COUNT, "0").equals("0")) {
            mNotificationCountDrawer.setVisibility(View.GONE);

            mUpdateAvailable.setVisibility(View.GONE);

            mNotificationCountToolbar.setVisibility(View.GONE);
        } else {
            mUpdateAvailable.setVisibility(View.GONE);

            mNotificationCountDrawer.setVisibility(View.VISIBLE);
            mBell.setVisibility(View.VISIBLE);
            mNotificationCountToolbar.setVisibility(View.VISIBLE);
            mNotificationCountDrawer.setText(sPref.getString(Constants.NOTIFICATION_COUNT, "0"));
            mNotificationCountToolbar.setText(sPref.getString(Constants.NOTIFICATION_COUNT, "0"));
        }


        Intent intents = new Intent(this, FusedLocationService.class);
        startService(intents);

        isCalling = false;
        registerReceiver(mReceiver, mIntentFilter);
        registerReceiver(mLocationReceiver, mIntentFilterLocation);
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter("com.koorca.notification_come"));


        if (mCamera == null) {
            try {

                int currentAPIVersion = Build.VERSION.SDK_INT;
                if (currentAPIVersion >= Build.VERSION_CODES.M) {
                    mCamera = null;
                    imageSavefalse = true;
                    mCamera = Camera.open();
                    Log.d(TAG, "surfaceCreated(), mCamera=" + mCamera);
                    mCamera.setDisplayOrientation(90);
                    try {
                        mCamera.setPreviewDisplay(mCameraPreview.getHolder());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Camera.Parameters params = mCamera.getParameters();
                    params.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
                    params.set("jpeg-quality", 90);
                    //  params.set("rotation", 90);
                    params.set("orientation", "portrait");
                    params.setPictureFormat(PixelFormat.JPEG);
                    Camera.Size bestSize = null;
                    List<Camera.Size> sizeList = mCamera.getParameters().getSupportedPreviewSizes();
                    bestSize = sizeList.get(0);
                    for (int i = 1; i < sizeList.size(); i++) {
                        if ((sizeList.get(i).width * sizeList.get(i).height) > (bestSize.width * bestSize.height)) {
                            bestSize = sizeList.get(i);
                        }
                    }

                    List<Integer> supportedPreviewFormats = params.getSupportedPreviewFormats();
                    Iterator<Integer> supportedPreviewFormatsIterator = supportedPreviewFormats.iterator();
                    while (supportedPreviewFormatsIterator.hasNext()) {
                        Integer previewFormat = supportedPreviewFormatsIterator.next();
                        if (previewFormat == ImageFormat.YV12) {
                            params.setPreviewFormat(previewFormat);
                        }
                    }

                    params.setPreviewSize(bestSize.width, bestSize.height);

                    params.setPictureSize(bestSize.width, bestSize.height);
                    mCamera.setParameters(params);
                    mCamera.setDisplayOrientation(90);
                    mCamera.startPreview();


                } else {

                    imageSavefalse = true;
                    mCamera = Camera.open();
                    Camera.Parameters params = mCamera.getParameters();
                    Camera.Size bestSize = null;
                    List<Camera.Size> sizeList = mCamera.getParameters().getSupportedPreviewSizes();
                    bestSize = sizeList.get(0);
                    for (int i = 1; i < sizeList.size(); i++) {
                        if ((sizeList.get(i).width * sizeList.get(i).height) > (bestSize.width * bestSize.height)) {
                            bestSize = sizeList.get(i);
                        }
                    }

                    List<Integer> supportedPreviewFormats = params.getSupportedPreviewFormats();
                    Iterator<Integer> supportedPreviewFormatsIterator = supportedPreviewFormats.iterator();
                    while (supportedPreviewFormatsIterator.hasNext()) {
                        Integer previewFormat = supportedPreviewFormatsIterator.next();
                        if (previewFormat == ImageFormat.YV12) {
                            params.setPreviewFormat(previewFormat);
                        }
                    }

                    params.setPreviewSize(bestSize.width, bestSize.height);

                    params.setPictureSize(bestSize.width, bestSize.height);
                    mCamera.setParameters(params);
                    setCameraDisplayOrientation(this,
                            Camera.CameraInfo.CAMERA_FACING_BACK, mCamera);
                    mCamera.setPreviewDisplay(mCameraPreview.getHolder());
                    if (mIsCapturing) {

                        mCamera.setDisplayOrientation(90);
                        mCamera.startPreview();
                        //  }
                    }
                }


            } catch (Exception e) {

            }
        }


    }


    public int setCameraDisplayOrientation(Activity activity,
                                           int cameraId, Camera camera) {
        Camera.CameraInfo info =
                new Camera.CameraInfo();
        Camera.getCameraInfo(cameraId, info);
        int rotation = activity.getWindowManager().getDefaultDisplay()
                .getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;
            case Surface.ROTATION_90:
                degrees = 90;
                break;
            case Surface.ROTATION_180:
                degrees = 180;
                break;
            case Surface.ROTATION_270:
                degrees = 270;
                break;
        }


        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;  // compensate the mirror
        } else {  // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }
        return result;
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mReceiver);
        unregisterReceiver(mLocationReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        if (mCamera != null) {
            mCamera.release();
            mCamera = null;
        }
    }


    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        if (mCamera != null) {
            try {
                mCamera.setPreviewDisplay(holder);
                if (mIsCapturing) {
                    mCamera.setDisplayOrientation(90);
                    mCamera.startPreview();
                }
            } catch (IOException e) {
                Toast.makeText(MainClass.this, "Unable to start camera preview.", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {


    }

    private void captureImage() {
        try {
            rotationImage();
            mCamera.takePicture(null, null, this);
        } catch (Exception e) {

        }

    }

    @Override
    public void onPictureTaken(byte[] data, Camera camera) {
        int rot = rotations;
        mCameraData = data;
        dateFormat1 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        date = new Date();
        sendDateToserver = dateFormat1.format(date);
        utcDate = CommonUtility.getDate(sendDateToserver);
        timeZone = TimeZone.getDefault().getDisplayName();

        Log.i("utcdate============", utcDate);

        try {
            if (totalImages()) {

                SyncClass sync = new SyncClass(MainClass.this, mCameraData, sendDateToserver, utcDate, timeZone, rot);
                sync.execute();
                setupImageCapture();
            } else {
                // Toast.makeText(MainActivity.this, "Limit Exceeded", Toast.LENGTH_SHORT).show();
                isLimitExceeded = true;
                imageSavefalse = true;

                mCamera.startPreview();
            }
        } catch (ParseException e) {
            e.printStackTrace();
        } finally {

        }
    }

    private void setupImageCapture() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            imageSavefalse = true;
            mCamera.stopPreview();
            Thread restart_preview = new Thread() {
                public void run() {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } finally {
                        try {
                            if (mCamera != null) {

                                mCamera.setDisplayOrientation(90);
                                mCamera.startPreview();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }
            };
            restart_preview.start();


        } else {
            mCamera.startPreview();
        }
        imageSavefalse = true;
        mCameraImage.setVisibility(View.INVISIBLE);
        mCameraPreview.setVisibility(View.VISIBLE);
        mCaptureImageButton.setOnClickListener(mCaptureImageButtonClickListener);
    }


    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals("response")) {
                String s = intent.getStringExtra("responses");
                Toast.makeText(MainClass.this, s, Toast.LENGTH_SHORT).show();

                canClick = true;

            }
        }
    };
    private BroadcastReceiver mLocationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("koorca.location")) {
                if (availableLocation) {
                    availableLocation = false;
                    Toast.makeText(MainClass.this, "You can now click pictures", Toast.LENGTH_SHORT).show();

                }
            }

        }
    };
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String message = intent.getStringExtra("notification_count");
            mBell.setVisibility(View.VISIBLE);
            mNotificationCountToolbar.setVisibility(View.VISIBLE);
            mNotificationCountDrawer.setVisibility(View.VISIBLE);
            mNotificationCountToolbar.setText(message);
            mNotificationCountDrawer.setText(message);

            Log.d("receiver", "Got message: " + message);
        }
    };


    boolean totalImages() throws ParseException {
        Date date1 = dateFormat.parse(currentDate);
        Date lastDate = null;
        if (sPref.getString(Constants.LAST_DATE, "").equals("")) {
            last_date = "";
        } else {
            last_date = sPref.getString(Constants.LAST_DATE, "");
            lastDate = dateFormat.parse(sPref.getString(Constants.LAST_DATE, ""));
        }
        if (date1.equals(lastDate) || last_date.equals("")) {
            if (sPref.getInt(Constants.COUNT, 0) == 25) {
                return false;
            } else {

                editor.putString(Constants.LAST_DATE, currentDate).commit();
                int newCurrent = sPref.getInt(Constants.COUNT, 0) + 1;


                editor.putInt(Constants.COUNT, newCurrent).commit();
                return true;


            }

        } else {
            editor.putInt(Constants.COUNT, 0).commit();
            int currentCount = sPref.getInt(Constants.COUNT, 0);
            editor.putString(Constants.LAST_DATE, currentDate).commit();
            editor.putInt(Constants.COUNT, currentCount++).commit();
            return true;


        }
    }

    @Override
    public void onBackPressed() {


    }

    public void rotationImage() {
        listener = new OrientationEventListener(this, SensorManager.SENSOR_DELAY_NORMAL) {
            @Override
            public void onOrientationChanged(int orientation) {
                rotations = ((orientation + 45) / 90) % 4;
            }
        };

        if (listener.canDetectOrientation()) {

            listener.enable();
        } else {
            Toast.makeText(this, "Can't DetectOrientation", Toast.LENGTH_LONG).show();
            finish();
        }
    }


    @Override
    public boolean onTouch(View view, MotionEvent event) {
        Camera.Parameters params = mCamera.getParameters();
        int action = event.getAction();


        if (event.getPointerCount() > 1) {
            // handle multi-touch events
            if (action == MotionEvent.ACTION_POINTER_DOWN) {
                mDist = getFingerSpacing(event);
            } else if (action == MotionEvent.ACTION_MOVE && params.isZoomSupported()) {
                mCamera.cancelAutoFocus();
                handleZoom(event, params);
            }
        } else {
            // handle single touch events
            if (action == MotionEvent.ACTION_UP) {
                handleFocus(event, params);
            }
        }
        return true;
    
    }
    private void handleZoom(MotionEvent event, Camera.Parameters params) {
        int maxZoom = params.getMaxZoom();
        int zoom = params.getZoom();
        float newDist = getFingerSpacing(event);
        if (newDist > mDist) {
            //zoom in
            if (zoom < maxZoom)
                zoom++;
        } else if (newDist < mDist) {
            //zoom out
            if (zoom > 0)
                zoom--;
        }
        mDist = newDist;
        params.setZoom(zoom);
        mCamera.setParameters(params);
    }

    public void handleFocus(MotionEvent event, Camera.Parameters params) {
        int pointerId = event.getPointerId(0);
        int pointerIndex = event.findPointerIndex(pointerId);
        // Get the pointer's current position
        float x = event.getX(pointerIndex);
        float y = event.getY(pointerIndex);

        List<String> supportedFocusModes = params.getSupportedFocusModes();
        if (supportedFocusModes != null && supportedFocusModes.contains(Camera.Parameters.FOCUS_MODE_AUTO)) {
            mCamera.autoFocus(new Camera.AutoFocusCallback() {
                @Override
                public void onAutoFocus(boolean b, Camera camera) {
                    // currently set to auto-focus on single touch
                }
            });
        }
    }

    /** Determine the space between the first two fingers */
    private float getFingerSpacing(MotionEvent event) {
        // ...
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);
        return (float)Math.sqrt(x * x + y * y);
    }

}

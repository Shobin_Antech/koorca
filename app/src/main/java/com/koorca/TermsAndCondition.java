package com.koorca;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Toast;

import com.koorca.customclasses.CustomBoldText;
import com.koorca.utilities.Constants;
import com.koorca.viewpager.ViewPager2;


/**
 * Created by sourav on 31/8/16.
 */
public class TermsAndCondition extends Activity {
    SharedPreferences sPref;
    SharedPreferences.Editor editor;
    CheckBox cb;
    CustomBoldText mContinue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.terms_conditions);
        sPref = getSharedPreferences(Constants.SHARED_PREF, MODE_PRIVATE);
        editor = sPref.edit();
        if(!sPref.getBoolean(Constants.UPDATE_NUMBER,false))
        ValidateNumber.fa.finish();
        cb = (CheckBox) findViewById(R.id.cb);
        mContinue = (CustomBoldText) findViewById(R.id.tvAgree);
        mContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cb.isChecked()) {
                    editor.putBoolean(Constants.TERMS_CONDITIONS, true).commit();
                    Intent i = new Intent(TermsAndCondition.this, ViewPager2.class);
                    startActivity(i);
                    finish();
                    overridePendingTransition(R.anim.slide_in, R.anim.slide_out);

                } else {
                    Toast.makeText(TermsAndCondition.this, "Please accept terms and condition", Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}

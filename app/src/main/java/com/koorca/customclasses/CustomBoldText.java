package com.koorca.customclasses;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by parikshit_shekhawat on 25/1/16.
 */
public class CustomBoldText extends TextView {
    private static final String TAG = "TextView";

    public CustomBoldText(Context context) {
        super(context);
        Typeface face= Typeface.createFromAsset(context.getAssets(), "font/ubuntu_bold.ttf");
        this.setTypeface(face);
    }

    public CustomBoldText(Context context, AttributeSet attrs) {
        super(context, attrs);
        Typeface face= Typeface.createFromAsset(context.getAssets(), "font/ubuntu_bold.ttf");
        this.setTypeface(face);
    }

    public CustomBoldText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        Typeface face= Typeface.createFromAsset(context.getAssets(), "font/ubuntu_bold.ttf");
        this.setTypeface(face);
    }

    protected void onDraw (Canvas canvas) {
        super.onDraw(canvas);


    }
}
package com.koorca.viewpager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.koorca.R;
import com.koorca.service.CountryName;
import com.koorca.service.SendDeviceToken;
import com.koorca.utilities.CommonUtility;
import com.koorca.utilities.Constants;


/**
 * Created by sourav on 30/8/16.
 */

public class ViewPager2 extends FragmentActivity {
    ViewPager pager;
    public static LinearLayout mPoints;
    private ImageView[] dots;
    private int dotsCount;
    RelativeLayout header_Rl;
    ImageView logo_Iv;
    boolean result = false;
    CommonUtility utility;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.viewpager2);
        startService(new Intent(ViewPager2.this, SendDeviceToken.class));

        utility = new CommonUtility(this);
        mPoints = (LinearLayout) findViewById(R.id.viewPagerCountDots2);
        pager = (ViewPager) findViewById(R.id.view_pager2);
        PagerAdapter1 pagerAdapter = new PagerAdapter1(getSupportFragmentManager());
        pager.setAdapter(pagerAdapter);
        setUiPageViewController();
        callService();
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                if (position == 0) {
                    mPoints.setVisibility(View.VISIBLE);

                    for (int i = 0; i < dotsCount; i++) {
                        dots[i].setImageDrawable(getResources().getDrawable(R.drawable.unselected_dot));

                    }
                    dots[position].setImageDrawable(getResources().getDrawable(R.drawable.selected_dot));
                } else if (position == 1) {
                    mPoints.setVisibility(View.VISIBLE);

                    for (int i = 0; i < dotsCount; i++) {
                        dots[i].setImageDrawable(getResources().getDrawable(R.drawable.unselected_dot));

                    }
                    Log.e("onPageSelected:", position + "");
                    dots[position].setImageDrawable(getResources().getDrawable(R.drawable.selected_dot));
                } else if (position == 2) {
                    mPoints.setVisibility(View.VISIBLE);

                    for (int i = 0; i < dotsCount; i++) {
                        dots[i].setImageDrawable(getResources().getDrawable(R.drawable.unselected_dot));

                    }
                    Log.e("onPageSelected:", position + "");
                    dots[position].setImageDrawable(getResources().getDrawable(R.drawable.selected_dot));
                } else if (position == 3) {

                    mPoints.setVisibility(View.VISIBLE);
                    for (int i = 0; i < dotsCount; i++) {
                        dots[i].setImageDrawable(getResources().getDrawable(R.drawable.unselected_dot));

                    }
                    Log.e("onPageSelected:", position + "");
                    dots[position].setImageDrawable(getResources().getDrawable(R.drawable.selected_dot));
                } else if (position == 4) {
                    mPoints.setVisibility(View.GONE);


                }

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void setUiPageViewController() {

        dotsCount = 4;
        dots = new ImageView[dotsCount];

        for (int i = 0; i < dotsCount; i++) {
            dots[i] = new ImageView(this);
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.unselected_dot));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );

            params.setMargins(4, 0, 4, 0);

            mPoints.addView(dots[i], params);
        }

        dots[0].setImageDrawable(getResources().getDrawable(R.drawable.selected_dot));
    }

    private void callService() {
        if (utility.checkInternetConection()) {
            startService(new Intent(ViewPager2.this, CountryName.class));
        } else {
            Toast.makeText(ViewPager2.this, getResources().getString(R.string.connect_to_internet), Toast.LENGTH_LONG).show();
        }

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


}




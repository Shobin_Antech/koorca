package com.koorca.viewpager;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.koorca.R;
import com.koorca.customclasses.CustomMediumText;

/**
 * Created by sourav on 5/9/16.
 */

public class PageTwo extends Fragment {
    View view;
    CustomMediumText text;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.pager_item_two, null);

        return view;
    }

}

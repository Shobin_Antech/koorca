package com.koorca.navigation_activities;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.koorca.MyApplication;
import com.koorca.R;
import com.koorca.customclasses.CustomBoldText;
import com.koorca.customclasses.CustomRegularText;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by sourav on 15/11/16.
 */

public class Notification_Detail extends ActivityToolbar {
    CustomBoldText mHeading;
    CustomRegularText mNotificationDetail;
    String messageId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout framelayout = (FrameLayout) findViewById(R.id.frameLayouts);
        framelayout.addView(getLayoutInflater().inflate(R.layout.notification_detail,
                null));
        mName.setText("Notifications Detail");
        mSettings.setVisibility(View.GONE);
        mNotificationCount.setVisibility(View.GONE);
        mBellIconNotification.setVisibility(View.GONE);
        mHeading=(CustomBoldText)findViewById(R.id.tvHeadings);
        mNotificationDetail=(CustomRegularText)findViewById(R.id.tvNotification);
        messageId=getIntent().getStringExtra("NOTIFICATION_ID");
        callAPI(messageId);

    }

    private void callAPI(String msg_id) {
        String url = "http://koorca.s4.staging-host.com/KOORCA/ReadMessage?messageId="+msg_id;


        StringRequest strReq = new StringRequest(Request.Method.GET,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("Result", response.toString());
                try {
                    parseResponse(response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("error", "Error: " + error.getMessage());

            }
        }) {


        };
        MyApplication.getInstance().addToRequestQueue(strReq, "get_otp");
    }

    private void parseResponse(String response) throws JSONException {
        JSONObject jsonObject=new JSONObject(response);
        if(jsonObject.getBoolean("status"))
        {
            JSONObject jsonObject1=jsonObject.getJSONObject("Obj");
            JSONObject jsonObject2=jsonObject1.getJSONObject("BroadcastMessage");
            mHeading.setText(jsonObject2.getString("Title"));
            mNotificationDetail.setText(jsonObject2.getString("Message"));


        }
    }
}

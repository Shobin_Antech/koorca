package com.koorca.viewpager;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

/**
 * Created by sourav on 27/9/16.
 */

public class PagerAdapter1 extends FragmentPagerAdapter {

    ArrayList<Fragment> fragments;
    public PagerAdapter1(FragmentManager fm) {
        super(fm);
        fragments=new ArrayList<Fragment>();
        fragments.add(new Page_Post1());
        fragments.add(new Page_Post2());
        fragments.add(new Page_Post3());
        fragments.add(new Page_Post4());
        fragments.add(new Page_Post5());


    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }
}
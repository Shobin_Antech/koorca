package com.koorca.fcm_classes;

import android.content.SharedPreferences;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.koorca.MyApplication;
import com.koorca.utilities.Constants;

/**
 * Created by Belal on 5/27/2016.
 */


//Class extending FirebaseInstanceIdService
public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";
    private String url;
    SharedPreferences sPref;
    SharedPreferences.Editor editor;

    @Override
    public void onCreate() {
        super.onCreate();
        sPref = getSharedPreferences(Constants.SHARED_PREF, MODE_PRIVATE);
        editor = sPref.edit();
    }

    @Override
    public void onTokenRefresh() {

        //Getting registration token
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        editor.putString(Constants.DEVICE_ID, refreshedToken).commit();

        //Displaying token on logcat
        Log.d(TAG, "Refreshed token: " + refreshedToken);


    }


}

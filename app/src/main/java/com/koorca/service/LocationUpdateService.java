package com.koorca.service;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.koorca.MyApplication;
import com.koorca.R;
import com.koorca.utilities.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by sourav on 17/11/16.
 */

public class LocationUpdateService extends Service {
    SharedPreferences sPref;
    String number, device_id, lat, lng;
    boolean notification_status;

    @Override
    public void onCreate() {
        super.onCreate();
        sPref = getSharedPreferences(Constants.SHARED_PREF, MODE_PRIVATE);
        number = sPref.getString(Constants.MOBILE_NUMBER, "");
        device_id = sPref.getString(Constants.DEVICE_ID, "");
        lat = sPref.getString(Constants.LATITUDE, "");
        lng = sPref.getString(Constants.LONGITUDE, "");
        notification_status = sPref.getBoolean(Constants.NOTIFICATION_STATUS,true);
        callAPI();
    }

    private void callAPI() {
        String url = "http://koorca.s4.staging-host.com/KOORCA/UpdateLocation";




        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("error", "Error: " + error.getMessage());

            }
        }) {

            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put("PhoneNumber",number);
                params.put("DeviceId",device_id);
                /*params.put("Lat", "-32.645746");
                params.put("Long", "134.917298");*/


                params.put("DeviceName", "android");
                params.put("Lat", lat);
                params.put("Lng",lng);
                params.put("IsDelete",notification_status+"");



                return params;
            }

        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(strReq, "profile_Pic");
    }

    private void parseResponse(String response) {
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}

package com.koorca.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.koorca.MyApplication;
import com.koorca.R;
import com.koorca.customclasses.CustomMediumText;
import com.koorca.customclasses.CustomRegularText;
import com.koorca.modal.NotificationModal;
import com.koorca.navigation_activities.Notification_Detail;
import com.koorca.utilities.CommonUtility;
import com.koorca.utilities.Constants;
import com.tubb.smrv.SwipeHorizontalMenuLayout;

import org.json.JSONException;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by sourav on 9/11/16.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyHolder> {
    Context ctx;
    ArrayList<NotificationModal> al;
    ChangeCount count;
    NoNotificationAvailable noNotification;

    public NotificationAdapter(Context ctx, ArrayList<NotificationModal> al, ChangeCount count, NoNotificationAvailable noNotification) {
        this.ctx = ctx;
        this.al = al;
        this.count = count;
        this.noNotification = noNotification;
    }

    @Override
    public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_item, null);
        MyHolder myHolder = new MyHolder(view);

        return myHolder;
    }

    @Override
    public void onBindViewHolder(final MyHolder holder, final int position) {


        if (al.get(position).isRead()) {
            holder.mMessage.setTextColor(ctx.getResources().getColor(R.color.grey));
            holder.mName.setTextColor(ctx.getResources().getColor(R.color.grey));
            holder.mTime.setTextColor(ctx.getResources().getColor(R.color.grey));
            holder.mDate.setTextColor(ctx.getResources().getColor(R.color.grey));
            holder.mMessage.setText(al.get(position).getMessage());
            holder.mName.setText(al.get(position).getTitle());
            String time = CommonUtility.getDateToLocal(al.get(position).getTime());
            holder.mTime.setText(time.substring(11,16));
            holder.mDate.setText(time.substring(0,10));
        } else {
            holder.mMessage.setTextColor(ctx.getResources().getColor(R.color.black));
            holder.mName.setTextColor(ctx.getResources().getColor(R.color.black));
            holder.mTime.setTextColor(ctx.getResources().getColor(R.color.black));
            holder.mDate.setTextColor(ctx.getResources().getColor(R.color.black));
            holder.mName.setTypeface(null, Typeface.BOLD);
            holder.mMessage.setTypeface(null, Typeface.BOLD);
            holder.mTime.setTypeface(null, Typeface.BOLD);
            holder.mDate.setTypeface(null, Typeface.BOLD);
            holder.mMessage.setText(al.get(position).getMessage());
            holder.mName.setText(al.get(position).getTitle());
            String time = CommonUtility.getDateToLocal(al.get(position).getTime());
            holder.mTime.setText(time.substring(11,16));
            holder.mDate.setText(time.substring(0,10));

        }
        if (al.get(position).isRead()) {
            holder.itemView.setBackgroundColor(ctx.getResources().getColor(R.color.white));
        } else {
            holder.itemView.setBackgroundColor(ctx.getResources().getColor(R.color.selected_color));
        }
        holder.mDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (al.get(position).isRead()) {

                } else {
                    count.countNotification();
                }
                holder.mainLayout.smoothCloseMenu();
                callAPI(al.get(position).getMessageId());

                al.remove(position);
                notifyDataSetChanged();
                if (al.size() == 0) {
                    noNotification.noNotification();
                }

            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (al.get(position).isRead()) {

                } else {
                    count.countNotification();
                }

                Intent i = new Intent(ctx, Notification_Detail.class);
                i.putExtra("NOTIFICATION_ID", al.get(position).getMessageId());
                ctx.startActivity(i);
                al.get(position).setRead(true);
                notifyDataSetChanged();
            }
        });

    }

    private void callAPI(String pos) {
        String url = "http://koorca.s4.staging-host.com/KOORCA/DeleteMessage?id=" + pos;


        StringRequest strReq = new StringRequest(Request.Method.GET,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("Result", response.toString());


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("error", "Error: " + error.getMessage());
            }
        }) {


        };
        MyApplication.getInstance().addToRequestQueue(strReq, "country_name");
    }

    @Override
    public int getItemCount() {
        return al.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {
        CircleImageView profilePic;
        CustomMediumText mName;
        CustomMediumText mMessage;
        CustomRegularText mTime;
        ImageView mDelete;
        View itemView;
        SwipeHorizontalMenuLayout mainLayout;
        CustomRegularText mDate;

        public MyHolder(View itemView) {
            super(itemView);

            this.itemView = itemView;
            profilePic = (CircleImageView) itemView.findViewById(R.id.image);
            mName = (CustomMediumText) itemView.findViewById(R.id.name);
            mMessage = (CustomMediumText) itemView.findViewById(R.id.message);
            mTime = (CustomRegularText) itemView.findViewById(R.id.time);
            mDate = (CustomRegularText) itemView.findViewById(R.id.date);
            mDelete = (ImageView) itemView.findViewById(R.id.ivDelete);
            mainLayout = (SwipeHorizontalMenuLayout) itemView.findViewById(R.id.sml);
        }
    }

    public interface ChangeCount {
        public void countNotification();
    }

    public interface NoNotificationAvailable {
        public void noNotification();
    }
}

package com.koorca.navigation_activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.koorca.MyApplication;
import com.koorca.R;
import com.koorca.adapters.FAQAdapter;
import com.koorca.modal.FAQModal;
import com.koorca.utilities.CommonUtility;
import com.koorca.utilities.CustomProgressDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by sourav on 10/11/16.
 */

public class User_Guide extends ActivityToolbar implements FAQAdapter.FAQ{
    CommonUtility utility;
    CustomProgressDialog dialog;
    RecyclerView mFAQ;
    RecyclerView.LayoutManager manager;
    RecyclerView.Adapter adapter;
    ArrayList<FAQModal> al = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout framelayout = (FrameLayout) findViewById(R.id.frameLayouts);
        framelayout.addView(getLayoutInflater().inflate(R.layout.user_guide,
                null));
        mName.setText("User Guide");
        mSettings.setVisibility(View.GONE);
        mNotificationCount.setVisibility(View.GONE);
        mBellIconNotification.setVisibility(View.GONE);
        utility = new CommonUtility(this);
        dialog = new CustomProgressDialog(this, "", false);
        mFAQ = (RecyclerView) findViewById(R.id.recyclerFAQ);
        manager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mFAQ.setLayoutManager(manager);

        if (utility.checkInternetConection()) {
            callAPI();

        } else {
            Toast.makeText(this, getResources().getString(R.string.connect_to_internet), Toast.LENGTH_SHORT).show();
        }
    }

    private void callAPI() {
        dialog.showDialog();

        String url = "http://koorca.s4.staging-host.com/KOORCA/GetFAQList";
        StringRequest strReq = new StringRequest(Request.Method.GET,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("Result", response.toString());
                try {
                    parseResponse(response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("error", "Error: " + error.getMessage());
                dialog.hideDialog();
            }
        }) {


        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(strReq, "country_name");
    }

    private void parseResponse(String response) throws JSONException {
        dialog.hideDialog();
        JSONObject jsonObject = new JSONObject(response);
        if (jsonObject.getBoolean("status")) {
            JSONArray jsonArray = jsonObject.getJSONArray("Obj");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                FAQModal modal = new FAQModal();
                modal.setQue(jsonObject1.getString("Question"));
                modal.setAns(jsonObject1.getString("Answer"));
                modal.setOpen(false);
                al.add(modal);
            }
            adapter = new FAQAdapter(this, al,this);
            mFAQ.setAdapter(adapter);
        } else {

        }
    }


    @Override
    public void selected() {

    }
}

package com.koorca;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;

import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.koorca.customclasses.CustomRegularText;
import com.koorca.navigation_activities.ActivityToolbar;
import com.koorca.navigation_activities.ContactUs;
import com.koorca.navigation_activities.MainClass;
import com.koorca.navigation_activities.NotificationClass;
import com.koorca.navigation_activities.Terms;
import com.koorca.navigation_activities.UpdateNumber;
import com.koorca.navigation_activities.User_Guide;
import com.koorca.utilities.Constants;

import me.leolin.shortcutbadger.ShortcutBadger;

public class MainActivity extends Activity implements OnClickListener {


    CustomRegularText mUserGuide, mContactUs, mUpdateProfile, mTerms, mNotification;
    RelativeLayout rlNotification, topRL;
    ImageView mCloseDrawer;
    DrawerLayout drawerLayout;
    private ImageView mOpen;
    NavigationView navigationView;
    SharedPreferences sPref;
    ImageView mBell;
    CustomRegularText mNewUpdation;
    SharedPreferences.Editor editor;
    String mCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main2);
        sPref = getSharedPreferences(Constants.SHARED_PREF, MODE_PRIVATE);
        editor = sPref.edit();
       mUserGuide = (CustomRegularText) findViewById(R.id.tvUserGuide);
        mContactUs = (CustomRegularText) findViewById(R.id.tvContactUs);
        mUpdateProfile = (CustomRegularText) findViewById(R.id.tvUpdateProfile);
        mTerms = (CustomRegularText) findViewById(R.id.tvTerms);
        rlNotification = (RelativeLayout) findViewById(R.id.relativeLayout);
        topRL = (RelativeLayout) findViewById(R.id.topRL);
        mCloseDrawer = (ImageView) findViewById(R.id.close_drawer);
        mBell = (ImageView) findViewById(R.id.ivBell);
        mNewUpdation = (CustomRegularText) findViewById(R.id.tvNotificationCountToolbar);
        mNotification = (CustomRegularText) findViewById(R.id.tvNotificationCountDrawer);
        mNotification.setText(sPref.getString(Constants.NOTIFICATION_COUNT, "0"));

        topRL.setOnClickListener(this);
        mCloseDrawer.setOnClickListener(this);
        mUserGuide.setOnClickListener(this);
        mContactUs.setOnClickListener(this);
        mUpdateProfile.setOnClickListener(this);
        mTerms.setOnClickListener(this);
        rlNotification.setOnClickListener(this);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        mOpen = (ImageView) findViewById(R.id.openNav);
        navigationView = (NavigationView) findViewById(R.id.nav_view);

        mOpen.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.openDrawer(Gravity.LEFT);
            }
        });
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.topRL:
                Intent i = new Intent(MainActivity.this, MainClass.class);
                startActivity(i);
                closeDrawer();
                break;
            case R.id.close_drawer:
                if (drawerLayout.isDrawerOpen(Gravity.LEFT)) {
                    drawerLayout.closeDrawers();
                }
                break;
            case R.id.tvUserGuide:
                Intent iUserGuide = new Intent(MainActivity.this, User_Guide.class);
                startActivity(iUserGuide);
                closeDrawer();

                break;
            case R.id.tvContactUs:
                Intent iContactUs = new Intent(MainActivity.this, ContactUs.class);
                startActivity(iContactUs);
                closeDrawer();
                break;
            case R.id.tvUpdateProfile:
                Intent iUpdate = new Intent(MainActivity.this, UpdateNumber.class);
                startActivity(iUpdate);
                closeDrawer();
                break;
            case R.id.tvTerms:
                Intent iTerms = new Intent(MainActivity.this, Terms.class);
                startActivity(iTerms);
                closeDrawer();
                break;
            case R.id.relativeLayout:
                Intent iNotification = new Intent(MainActivity.this, NotificationClass.class);
                startActivity(iNotification);
                closeDrawer();
                break;
        }
    }

    void closeDrawer() {
        if (drawerLayout.isDrawerOpen(Gravity.LEFT)) {
            drawerLayout.closeDrawers();
        }
    }
    @Override
    protected void onPause() {
        super.onPause();
        boolean success = ShortcutBadger.removeCount(MainActivity.this);
        if(success)
        {

        }
        else
        {
            ShortcutBadger.removeCount(getApplicationContext());
        }
    }
}
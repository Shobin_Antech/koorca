package com.koorca.viewpager;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.koorca.R;

import java.util.ArrayList;

/**
 * Created by sourav on 5/9/16.
 */

public class MainViewPager extends FragmentActivity {
    ArrayList<Fragment> fragments;
    ArrayList<Fragment> fList;
    public static  LinearLayout pager_indicator;
    ViewPager pager;
    int dotsCount;
    ImageView[] dots;
    public static RelativeLayout mTop;
    public static ImageView mLogo;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.view_pager_activity);
        pager_indicator = (LinearLayout) findViewById(R.id.viewPagerCountDots);
        mLogo=(ImageView)findViewById(R.id.logo);
        mTop = (RelativeLayout) findViewById(R.id.topRL);
        fragments = getFragments();

        pager = (ViewPager) findViewById(R.id.view_pager);
        pager.setOffscreenPageLimit(0);
        pager.setAdapter(new MyPagerAdapter(getSupportFragmentManager()));
        pager.setOnPageChangeListener(myOnPageChangeListener);
        setUiPageViewController();


    }

    ViewPager.OnPageChangeListener myOnPageChangeListener =
            new ViewPager.OnPageChangeListener() {

                @Override
                public void onPageScrollStateChanged(int state) {
                    //Called when the scroll state changes.
                }

                @Override
                public void onPageScrolled(int position,
                                           float positionOffset, int positionOffsetPixels) {
                    //This method will be invoked when the current page is scrolled,
                    //either as part of a programmatically initiated smooth scroll
                    //or a user initiated touch scroll.


                }

                ;

                @Override
                public void onPageSelected(int position) {
                    //This method will be invoked when a new page becomes selected.

                    for (int i = 0; i < dotsCount; i++) {
                        dots[i].setImageDrawable(getResources().getDrawable(R.drawable.unselected_dot));

                    }
                    Log.e("onPageSelected:", position + "");
                    dots[position].setImageDrawable(getResources().getDrawable(R.drawable.selected_dot));
                }
            };


    public ArrayList<Fragment> getFragments() {
        /*fList = new ArrayList<Fragment>();
        fList.add(PageOne.newInstance());
        fList.add(PageTwo.newInstance());

        fList.add(PageThree.newInstance());

        fList.add(PageFour.newInstance());
        fList.add(PageFive.newInstance());

        fList.add(PageSix.newInstance());
        fList.add(PageSeven.newInstance());*/


        return fList;
    }

    private void setUiPageViewController() {


        dotsCount = fragments.size();
        dots = new ImageView[dotsCount];

        for (int i = 0; i < dotsCount; i++) {
            dots[i] = new ImageView(this);
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.unselected_dot));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );

            params.setMargins(4, 0, 4, 0);

            pager_indicator.addView(dots[i], params);
        }

        dots[0].setImageDrawable(getResources().getDrawable(R.drawable.selected_dot));
    }


    private class MyPagerAdapter extends FragmentPagerAdapter {

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int pos) {
            return fragments.get(pos);


        }

        @Override
        public int getCount() {
            return fragments.size();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }
}
